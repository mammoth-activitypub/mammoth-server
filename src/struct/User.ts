'use strict';

export interface User {
  userId: string;
  password: string;
  hashedPassword: string;
  email: string;
  userDb: string;

  publicKey: string;
  privateKey: string;
  admin: boolean;
}
