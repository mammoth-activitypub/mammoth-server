'use strict';
/// <reference types="hapi" />

import {Request, ResponseToolkit} from 'hapi';
import {User} from '../struct/User';
import {Activity, ASObject, OrderedCollection} from 'mammoth-activitystreams';
const { ObjectType } = require('mammoth-activitystreams');

export {};

const Boom = require('@hapi/boom');
const Content = require('../db/content');
const ActivityStreamsHandler = require('../service/activity-dispatcher');
const Users = require('../service/users');


module.exports = {
  init: (server: any): void => {
    // incoming message from our user
    /**
     * @api        {post} /a/{actor}/outbox Accept outbound message
     * @apiName    Send a message from the user
     * @apiGroup   C2S
     * @apiDescription This implements the C2S interface for accepting objects from a user for delivery.
     *                 The message may or may not be an Activity (it will be wrapped
     *                 in one if not), but must include recipients, attribution, actor, object,
     *                 and all other necessary information. The message will be assigned an ID,
     *                 saved, accepted, and queued
     *                 for delivery (but not yet delivered) before returning.
     *
     * @apiParam {String} actor The actor who is sending the message. Must be the authenticated user.
     * @apiParam {json} body The Activity or Object that is to be sent.
     * @apiParamExample {json} Note:
     * {
     *   "type": "Note",
     *   "attributedTo": "https://mammoth.example.com/a/bob",
     *   "content": "Hello, world!",
     *   "to": "https://mammoth.example.com/a/bob/followers"
     * }
     * @apiParamExample {json} Activity:
     * {
     *   "type": "Create",
     *   "actor": "https://mammoth.example.com/a/bob",
     *   "to": "https://mammoth.example.com/a/bob/followers",
     *   "object": {
     *     "type": "Note",
     *     "attributedTo": "https://mammoth.example.com/a/bob",
     *     "content": "Hello, world!",
     *     "to": "https://mammoth.example.com/a/bob/followers"
     *   }
     * }
     */
    server.route({
      method: 'POST',
      path: '/a/{actor}/outbox',
      handler: async (request: Request, h: ResponseToolkit) => {
        const user = request.auth.credentials as User;
        const isOwner = user.userId === request.params.actor;
        if (!isOwner) {
          return Boom.forbidden('not allowed to post messages to someone else‘s outbox');
        }

        const doc = request.payload as Activity;
        try {
          const actorId = Users.getActorDocumentIdFromActorId(user.userId);
          const actor = await Content.fetchObject(actorId, null, user);
          await ActivityStreamsHandler.handlePostedDocument(user, actor, doc);
          // noinspection TypeScriptValidateJSTypes
          return h.response(doc)
            .header('Location', doc.id)
            .code(201);
        } catch (e) {
          if (Boom.isBoom(e)) {
            return e;
          }
          return Boom.badImplementation(e);
        }
      }
    });

    /**
     * @api        {get} /a/{actor}/outbox Public outbox
     * @apiName    Get an actor‘s public timeline
     * @apiGroup   public
     * @apiDescription This implements the C2S interface for fetching an actor's public timeline.
     *                 It returns an OrderedPagedCollection of every public Activity the actor has
     *                 submitted, in reverse chronological order.
     *
     * @apiParam {String} actor The actor whose outbox is being fetched. Actor must exist.
     *
     * @apiSuccess {json} OrderedCollectionPage A page of an OrderedPagedCollection of the actor's public activities.
     */
    // noinspection TypeScriptValidateJSTypes
    server.route({
      method: 'GET',
      path: '/a/{actor}/outbox',
      options: {
        auth: {
          mode: 'optional'
        }
      },
      handler: async (request: Request) => {
        const requestedUser = await Users.findUser(request.params.actor);
        const authenticatedUser = request.auth.credentials as User;
        if (authenticatedUser && authenticatedUser.userId === request.params.actor) {
          return Content.timeline(authenticatedUser, 10);
        }

        if (requestedUser) {
          const currentOffset = request.params.offset || 0;
          const result = await Content.publicTimeline(requestedUser, currentOffset, 10);
          let nextPage = undefined;
          if (result.total_rows > currentOffset + result.rows.length) {
            nextPage = Content.getActorDocumentIdFromActorId(requestedUser.userId) +
              '/outbox?offset=' + (currentOffset + result.rows.length);
          }
          return {
            '@context': 'https://www.w3.org/ns/activitystreams',
            type: ObjectType.OrderedCollectionPage,
            totalItems: result.total_rows,
            next: nextPage,
            orderedItems: result.rows.map((row: any) => row.doc)
          } as OrderedCollection<ASObject>;
        }

        Boom.notFound('Actor ' + request.params.actor + ' not found');
      }
    });
  }
};
