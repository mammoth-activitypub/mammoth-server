'use strict';
/// <reference types="hapi" />

import {User} from '../struct/User';
import {Actor} from 'mammoth-activitypub';
import {ASObject, ObjectRef, ObjectRefs} from 'mammoth-activitystreams';
const { resolveObjectRefs } = require('mammoth-activitystreams');
import {APObject} from 'mammoth-activitypub';
import {Request, ResponseObject, ResponseToolkit} from 'hapi';
const https = require('https');

export {};

const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');
const Users = require('../service/users');
const Content = require('../db/content');
const Config = require('config');
const Axios = require('axios');
const InboxRoutes = require('./inbox-routes');

function transferHeaders(from: any, to: any, headers: string[]): void{
  for (const index in headers) {
    const header = headers[index];
    const value = from[header];
    if (value) {
      to[header] = value;
    }
  }
}

function addHeaders(response: ResponseObject, incomingResponseHeaders: {}, headers: string[]): ResponseObject {
  headers.forEach((header: string) => {
    // @ts-ignore
    const value = incomingResponseHeaders[header];
    if (value) {
      response = response.header(header, value);
    }
  });
  return response;
}

function checkFieldFor(refs: ObjectRefs<any>, addressee: string): boolean {
  if (!refs) {
    return false;
  }
  const resolved = resolveObjectRefs(refs);
  const predicate = (ref: string): boolean => ref === addressee;
  if (typeof resolved === "string") {
    return predicate(resolved);
  }
  const refsArray = resolved as string[];
  return !!refsArray.find(predicate);
}

const publicAddressee = 'https://www.w3.org/ns/activitystreams#Public';
function addresseeIs(object: APObject, addressee: string): boolean {
  return checkFieldFor(object.to, addressee) ||
    checkFieldFor(object.cc, addressee) ||
    checkFieldFor(object.bto, addressee) ||
    checkFieldFor(object.bcc, addressee) ||
    checkFieldFor(object.audience, addressee);
}

async function confirmAuthorization(user: User, object: APObject, request: Request): Promise<void> {
  if (addresseeIs(object, publicAddressee)) {
    return;
  }

  if (request.headers.signature) {
    const actor = await InboxRoutes.verifyHttpSignatures(user, request);
    if (addresseeIs(object, actor)) {
      return;
    }
    // should check collections, but this is enough for now
  }

  throw Boom.unauthorized('not authorized to view object ' + object.id);
}

interface WFLink {
  rel: string;
  type: string;
  href: string;
}

function findActivityPubLink(links: WFLink[]): WFLink {
  if (links.length === 1) {
    return links[0];
  }
  return links.find((link: WFLink) => link.type === 'application/activity+json');
}

async function webFingerSearch(query: string): Promise<ASObject[]> {
  if (query.indexOf('@') > 0 && query.indexOf('@') < query.length) {
    // webfinger query
    const hostPart = query.match(/^.*@(.*)$/)[1];
    const allowSelfSignedCerts = hostPart.startsWith('localhost');
    const agent = new https.Agent({
      rejectUnauthorized: !allowSelfSignedCerts
    });
    try {
      const url = `https://${hostPart}/.well-known/webfinger`;
      const result = await Axios.default.get(
        url,
        {
          headers: {
            'Accept': 'application/json'
          },
          params: {
            resource: `acct:${query}`
          },
          httpsAgent: agent
        });
      const actor: Actor = (await Axios.default.get(findActivityPubLink(result.data.links).href, {
        headers: {
          'Accept': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
        },
        httpsAgent: agent
      })).data;
      return [actor];
    } catch (e) {
      // nothing to do here
      console.info(`webfinger search for ${query} failed: ${e.message}`);
    }
  }
  return [];
}

function isActivityPubContentType(contentType: string): boolean {
  const activityPubContentTypes = [
    'application/activity+json',
    'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
  ];
  return activityPubContentTypes.includes(contentType);
}

async function urlSearch(query: string): Promise<ObjectRef<ASObject>[]> {
  const contentLinks = query.match(/(?:(?:https?):\/\/|www\.)(?:\([-A-Z0-9+&@#/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#/%=~_|$?!:,.]*\)|[A-Z0-9+&@#/%=~_|$])/igm);
  if (contentLinks) {
    const objects = [] as ObjectRef<ASObject>[];
    await Promise.all(contentLinks.map(async (url: string) => {
      try {
        const result = await Axios.default.get(url, {
          headers: {
            'Accept': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
          }
        });
        if (isActivityPubContentType(result.headers['content-type'])) {
          objects.push(result.data);
        }
      } catch (e) {
        console.error(e.message);
      }
    }));
    return objects;
  }
  return [];
}

/**
 * Routes for getting a user and their various collections.
 */
module.exports = {
  init: (server: any): void => {
    /**
     * @api        {get} /.well-known/webfinger Process webfinger request
     * @apiName    WebFinger
     * @apiGroup   public
     *
     * @apiParam   {String} resource The resource being identified e.g., acct:bob@mammoth.example.com
     *
     * @apiSuccess {Object} The webfinger result
     */
    server.route({
      method: 'GET',
      path: '/.well-known/webfinger',
      options: {
        auth: false,
        validate: {
          query: {
            resource: Joi.string().pattern(/^acct:(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@.*$/)
          }
        }
      },
      handler: (request: Request, h: ResponseToolkit) => {
        const resource = request.query.resource as string;
        const resourceParts = resource.match(/^acct:(\w+)@.*$/);
        const actorId = resourceParts[1];
        const actor = Content.getActorDocumentIdFromActorId(actorId);
        const host = new URL(actor).host;
        const requestedHost = resource.match(/^.*@(.*)$/)[1];
        if (requestedHost !== host) {
          throw Boom.badRequest('cannot serve webfinger for resource ' + resource);
        }
        const response = {
          subject: resource,
          links: [
            {
              rel: 'self',
              type: 'application/activity+json',
              href: actor
            },
            {
              rel: 'http://webfinger.net/rel/profile-page',
              type: 'text/html',
              href: Content.getActorDocumentIdFromActorId(actorId)
            }
          ]
        };
        return h.response(response);
      }
    });

    /**
     * @api        {get} /api/search Search for users, tags, etc.
     * @apiName    Search
     * @apiGroup   user
     *
     * @apiParam   {String} q What you want to search on
     *
     * @apiSuccess {Object} The search result
     */
    server.route({
      method: 'GET',
      path: '/api/search',
      handler: async (request: Request) => {
        const query: string = request.query.q as string;
        const promises: Promise<any>[] = [
          webFingerSearch(query),
          urlSearch(query),
          //userSearch(query),

        ];
        const [ webfingerResults, urlResults ] = await Promise.all(promises);
        return { webfingerResults, urlResults };
      }
    });

    server.route({
      method: 'GET',
      path: '/api/proxy',
      handler: async(incomingRequest: Request, h: ResponseToolkit) => {
        const query: string = incomingRequest.query.q as string;
        const outgoingRequestHeaders = { };
        transferHeaders(incomingRequest.headers, outgoingRequestHeaders, ['accept', 'if-modified-since']);
        const incomingResponse = await Axios.get(query, {
          headers: outgoingRequestHeaders
        });
        let response = h.response(incomingResponse.data);
        response = addHeaders(response, incomingResponse.headers, ['etag', 'content-type', 'last-modified', 'cache-control']);
        return response;
      }
    });

    server.route({
      method: 'GET',
      path: '/api/proxy-head',
      handler: async(incomingRequest: Request, h: ResponseToolkit) => {
        const query: string = incomingRequest.query.q as string;
        const outgoingRequestHeaders = { };
        transferHeaders(incomingRequest.headers, outgoingRequestHeaders, ['accept', 'if-modified-since']);
        try {
          const incomingResponse = await Axios.head(query, {
            headers: outgoingRequestHeaders
          });
          let response = h.response(incomingResponse.data);
          response = addHeaders(response, incomingResponse.headers, ['etag', 'content-type', 'last-modified', 'cache-control']);
          return response;
        } catch (e) {
          throw Boom.badGateway('unable to head document', e);
        }
      }
    });

    /**
     * @api        {get} /a/{actor} Get an Actor object for an actor
     * @apiName    Actor
     * @apiGroup   public
     *
     * @apiParam   {String} actor The actorId of the actor for which you want an actor object.
     *
     * @apiSuccess {Object} The Actor's object
     */
    server.route({
      method: 'GET',
      path: '/a/{actor}',
      options: {
        auth: {
          mode: 'optional'
        }
      },
      handler: async (request: Request, h: ResponseToolkit) => {
        const actorId = request.params.actor;
        let key = Content.getActorDocumentIdFromActorId(actorId);
        const user: User = await Users.findUser(actorId);
        const actor: Actor = await Content.getById(user, key);
        if (!actor) {
          throw Boom.notFound();
        }
        actor["@context"] = ["https://www.w3.org/ns/activitystreams", "https://w3id.org/security/v1"];
        actor.followers = key + '/followers';
        actor.following = key + '/following';
        actor.liked = key + '/liked';
        actor.likes = key + '/likes';
        actor.shares = key + '/shares';
        let response;
        const anyObject = actor as any;
        if (anyObject._rev) {
          response = h.entity({etag: anyObject._rev});
        }
        if (!response) {
          delete anyObject._id;
          delete anyObject._rev;
          return h.response(actor)
            .type('application/json+ld; profile="https://www.w3.org/ns/activitystreams"');
        }
      }
    });

    const actorCollections = ['followers', 'following', 'likes', 'liked', 'shares'];
    /**
     * @api        {get} /a/{actor}/{id} Get an object from a user.
     * @apiName    Object
     * @apiGroup   public
     *
     * @apiParam   {String} actor The actor from whom you'll get an object
     * @apiParam   {String} id The the object's identifier
     *
     * @apiSuccess {Object} The object
     */
    server.route({
      method: 'GET',
      path: '/a/{user}/{id}',
      options: {
        auth: false
      },
      handler: async (request: Request, h: ResponseToolkit) => {
        const userId = request.params.user;
        const id = request.params.id;
        const key = Config.app.baseUrl + request.path; // fixme this is going away
        let object: APObject;
        try {
          const user = await Users.findUser(userId);
          if (actorCollections.includes(id)) {
            object = await Content.getCollectionForActor(user, id);
          } else {
            object = await Content.getById(user, key);
            if (!object) {
              // noinspection ExceptionCaughtLocallyJS
              throw Boom.notFound();
            }
            await confirmAuthorization(user, object, request);
            object.likes = object.id + '/likes';
            object.replies = object.id + '/replies';
            object.dislikes = object.id + '/dislikes';
            object.shares = object.id + '/shares';
            delete object.bcc;
            delete object.bto;
          }
        } catch (e) {
          throw Boom.boomify(e, {statusCode: e.statusCode});
        }
        object['@context'] = 'https://www.w3.org/ns/activitystreams';
        let response;
        const anyObject = object as any;
        if (anyObject._rev) {
          response = h.entity({etag: anyObject._rev});
        }
        if (!response) {
          delete anyObject._id;
          delete anyObject._rev;
          return h.response(object)
            .type('application/json+ld; profile="https://www.w3.org/ns/activitystreams"');
        }
      }
    });

  }
};
