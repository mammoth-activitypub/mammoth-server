'use strict';
/// <reference types="hapi" />

import {User} from '../struct/User';
import {Request, ResponseToolkit} from 'hapi';
import {Actor} from 'mammoth-activitypub';
import {
  ASObject,
  Link,
  ObjectRefs,
  OrderedCollectionPage,
  Page,
  Update
} from 'mammoth-activitystreams';
const {CollectionSubTypes, ObjectType} = require('mammoth-activitystreams');
import {FetchInstructions} from 'mammoth-api';
const Axios = require('axios');
import {Notification} from 'mammoth-api';

export {};

const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');
let Content = require('../db/content');
let Config = require('config');
let Users = require('../service/users');
let ActivityTransport = require('../service/activity-transport');
const JoiValidations = require('./joi-validations');
const Mercury = require('@postlight/mercury-parser');
const Notifications = require('../db/Notifications');
const Ioredis = require('ioredis');

const ioredis = Config.redis?.connection ? new Ioredis(Config.redis.connection) : null;

enum FetchInstruction {
  Resolve = 'Resolve',
  PopulateCollection = 'PopulateCollection'
}

function updateField(fieldName: string, src: any, dest: any): boolean {
  if (src[fieldName]) {
    dest[fieldName] = src[fieldName];
    return true;
  } else if (src[fieldName] === null) {
    delete dest[fieldName];
    return true;
  }
  return false;
}

async function createLinkPreview(url: string): Promise<ASObject | Link> {
  const requestHeaders = {
    Accept: 'application/activity+json, text/html, */*'
  };
  const contentType = (await Axios.head(url, { headers: requestHeaders })).headers['content-type'];
  if (contentType.search('text/html') !== -1) {
    const parsed = await Mercury.parse(url);
    const urls = [
      {
        type: 'Link',
        href: url,
        mediaType: contentType
      }
    ] as any[];
    if (parsed.lead_image_url) {
      const imageType = (await Axios.head(parsed.lead_image_url)).headers['content-type'];
      urls.push({
        type: 'Link',
        href: parsed.lead_image_url,
        mediaType: imageType
      })
    }
    return {
      id: parsed.url,
      type: ObjectType.Page,
      url: urls as unknown as ObjectRefs<ASObject>,
      summary: parsed.exerpt,
      name: parsed.title,
      mediaType: contentType
    } as Page;
  }
  return {
    type: 'Link',
    mediaType: contentType,
    href: url
  } as Link;
}

module.exports = {
  init: (server: any): void => {

    /**
     * @api        {post} /api/user Create user
     * @apiName    Create a new user account
     * @apiGroup   user
     * @apiDescription An API for creating new user accounts.
     *
     * @apiParam {json} body The user being created
     * @apiParam {String} body.userId The userId
     * @apiParam {String} body.password The cleartext password
     * @apiParam {String} body.email The user's email address
     */
    server.route({
      method: 'POST',
      path: '/api/user',
      options: {
        auth: false,
        validate: {
          payload: Joi.object({
            userId: JoiValidations.userIdValidation,
            password: JoiValidations.passwordValidation,
            email: JoiValidations.emailValidation
          })
        }
      },
      handler: async (request: Request, h: ResponseToolkit) => {
        try {
          return await Users.createUser(request.payload as User);
        } catch (e) {
          return h.response({message: e.message}).code(e.statusCode);
        }
      }
    });

    /**
     * @api        {get} /api/user Get user
     * @apiName    Get the logged-in user’s object
     * @apiGroup   user
     * @apiDescription Gets the User object for the currently logged-in user. The
     *                 <code>hashedPassword</code>, <code>privateKey</code>, and
     *                 <code>userDb</code> fields are redacted from the response.
     *
     * @apiSuccess {json} user The User object for the currently logged-in user.
     */
    server.route({
      method: 'GET',
      path: '/api/user',
      handler: async (request: Request) => {
        // @ts-ignore
        const user = await Users.findUser(request.auth.credentials.userId);
        delete user.hashedPassword;
        delete user.privateKey;
        delete user.userDb;

        return user;
      }
    });

    /**
     * @api        {post} /api/login Login
     * @apiName    Authenticate a user
     * @apiGroup   user
     * @apiDescription Authenticate a userId and password, returning the Actor object
     *                 if successful.
     *
     * @apiSuccess {json} actor The user's Actor object
     * @apiError (Error 401) AuthenticationFailed Unable to authenticate userId/password pair.
     */
    server.route({
      method: 'POST',
      path: '/api/login',
      options: {
        auth: {
          mode: 'try'
        },
        validate: {
          payload: Joi.object({
            userId: JoiValidations.userIdValidation,
            password: JoiValidations.passwordValidation
          })
        }
      },
      handler: async (request: Request) => {
        // @ts-ignore
        const userId = request.payload.userId;
        // @ts-ignore
        const password = request.payload.password;
        const user = await Users.authenticate(userId, password);
        if (user) {
          // @ts-ignore
          request.cookieAuth.set({ userId });
          const actorId = Users.getActorDocumentIdFromActorId(userId);
          const actor = await Content.getById(user, actorId);
          if (!actor) {
            throw Boom.badImplementation('actor not found in local database: ' + actorId);
          }
          return actor;
        }
        throw Boom.unauthorized('login unsuccessful');
      }
    });

    /**
     * @api        {post} /api/logout Logout
     * @apiName    Logout User
     * @apiGroup   user
     * @apiDescription Logs out the current user
     *
     * @apiSuccess {json} response
     */
    server.route({
      method: 'GET',
      path: '/api/logout',
      handler: async (request: Request, h: ResponseToolkit) => {
        // @ts-ignore
        request.cookieAuth.clear();
        return h.redirect('/');
      }
    });

    /**
     * @api        {patch} /api/user Update user
     * @apiName    Update a user
     * @apiGroup   user
     * @apiDescription Update field's in the user's User & Actor object.
     *
     * @apiParam {json} body The updates to apply
     * @apiParam {String} body.name The user's full name
     * @apiParam {String} body.email The user's email address
     * @apiParam {Object} body.icon The user's avatar
     * @apiParam {String} body.icon.mediaType The avatar's media type
     * @apiParam {String} body.icon.type The avatar's ActivityPub type; must be <code>Image</code>
     * @apiParam {String} body.icon.url The avatar's image url.
     *
     * @apiSuccess (Success 201) {json} Actor The user's updated actor object
     */
    server.route({
      method: 'PATCH',
      path: '/api/user',
      options: {
        validate: {
          payload: Joi.object({
            name: Joi.string(),
            email: Joi.string().email(),
            icon: Joi.object({
              mediaType: Joi.string().valid('image/jpeg', 'image/png', 'image/gif', 'image/svg'),
              type: Joi.string().valid('Image'),
              url: Joi.string().uri({ scheme: 'https'})
            }),
            autoFollowBack: Joi.boolean(),
            acceptMessagesFromFollowed: Joi.boolean()
          })
        }
      },
      handler: async (request: Request, h: ResponseToolkit) => {
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        let key = Content.getActorDocumentIdFromActorId(userId);
        const user: User = await Users.findUser(userId);
        let actor: Actor = await Content.getById(user, key);
        if (!actor) {
          throw Boom.notFound();
        }
        const patch = request.payload as any;
        let updateActor;
        let updateUser;
        updateActor = updateField('icon', patch, actor);
        updateActor = updateField('name', patch, actor) || updateActor;
        updateUser = updateField('email', patch, user);
        updateUser = updateField('autoFollowBack', patch, user) || updateUser;
        updateUser = updateField('acceptMessagesFromFollowed', patch, user) || updateUser;
        const promises = [];
        if (updateActor) {
          promises.push(Content.save(user, key, actor));
        }
        if (updateUser) {
          promises.push(Users.saveUser(user));
        }
        await Promise.all(promises);
        actor = await Content.getById(user, key);
        const activity: Update = {
          type: ObjectType.Update,
          actor: key,
          object: actor,
          to: key + '/followers'
        };
        await ActivityTransport.sendActivity(userId, activity);
        return h.response(actor).type('application/ld+json').code(201);
      }
    });

    /**
     * @api        {get} /api/fetch Fetch objects
     * @apiName    Fetch ActivityPub objects
     * @apiGroup   user
     * @apiDescription Used to fetch needed ActivityPub objects. Will attempt to fetch objects
     *   from Redis, from CouchDB, and finally from the remote server. FetchInstructions will be
     *   followed (if supplied) to resolve IDs of fields of this object. Resulting object will be
     *   cached if not a collection.
     *
     * @apiParam {String} id The ActivityPub ID of the object to fetch.
     * @apiParam {json} fetch The JSON fetchInstructions to use to fill out the requested object's fields.
     *
     * @apiSuccess {json} Object The requested object, with any requested fields populated.
     */
    server.route({
      method: 'GET',
      path: '/api/fetch',
      options: {
        // validate: {
        //   query: Joi.object({
        //     id: Joi.string().uri().required(),
        //     query: Joi.object().unknown() // todo fill out
        //   })
        // }
      },
      handler: async (request: Request, h: ResponseToolkit) => {
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        const user: User = await Users.findUser(userId);
        const id = request.query.id as string;
        const query = request.query.fetch ? JSON.parse(request.query.fetch as string) as FetchInstructions : null;
        const result = await Content.resolveObject(id, query, user);
        let response = h.response(result);
        if (result && !CollectionSubTypes.includes(result.type)) {
          response = response.header('Cache-Control', 'max-age=30');
        }
        return response;
      }
    });

    /**
     * @api        {get} /api/link-preview Link Preview
     * @apiName    Generate a link preview
     * @apiGroup   user
     * @apiDescription Given a URL, returns an ActivityPub object representing a preview of the
     *   document the URL refers to. If an HTML document, will return a <code>Page</code> object; if
     *   an image or a video, will return an <code>Link</code> object.
     *
     * @apiParam {String} url The URL of the object to be previewed.
     * @apiSuccess {json} An ActivityPub object representing the document returned by the URL.
     * @apiSuccessExample {json} HTML:
     * {
     *   "id": "https://www.frobotz.com/",
     *   "type": "Page",
     *   "url": "https://www.frobotz.com/",
     *   "summary": "The frobotz company makes the finest widgets…",
     *   "name: "Frobotz, Inc.",
     *   "mediaType": "text/html"
     * }
     * @apiSuccessExample {json} image/video:
     * {
     *   "type: "Link",
     *   "mediaType": "image/jpeg",
     *   "href": "https://www.frobotz.com/logo.jpeg"
     * }
     */
    server.route({
      method: 'GET',
      path: '/api/link-preview',
      options: {
        validate: {
          query: Joi.object({
            url: Joi.string().uri().required()
          })
        }
      },
      handler: async (request: Request) => {
        const url = request.query.url;
        // @ts-ignore
        const previewName = 'link-preview/' + url;
        let preview = await ioredis.get(previewName);
        if (!preview) {
          // could cache these for much better performance
          try {
            preview = await createLinkPreview(request.query.url as string);

            await ioredis.set(previewName, JSON.stringify(preview), 'EX', 86400);
          } catch (e) {
            return null;
          }
        } else {
          preview = JSON.parse(preview);
        }
        return preview;
      }
    });

    /**
     * @api        {get} /api/notifications Notifications
     * @apiName    Get the user’s notifications
     * @apiGroup   user
     * @apiDescription Gets the first page of the user’s most recent notifications, in reverse
     *   chronological order.
     *
     * @apiSuccess {json} body A collection of Notification objects.
     * @apiSuccessExample {json} Success:
     * {
     *   "type": "OrderedCollectionPage",
     *   "totalItems": 74,
     *   "unreadItems": 0,
     *   "orderedItems": [
     *     ...
     *   ]
     * }
     */
    server.route({
      method: 'GET',
      path: '/api/notifications',
      handler: async (request: Request) => {
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        const user: User = await Users.findUser(userId);
        const offset: string = request.query.offset as string || '0';
        const length: string = request.query.length as string || '5';
        const notificationsResult = await Notifications.getNotifications(user, Number.parseInt(offset), Number.parseInt(length));
        const fetchInstructions: FetchInstructions = {
          fetch: {
            object: {
              type: 'Object',
              fetch: {
                attributedTo: FetchInstruction.Resolve,
                actor: FetchInstruction.Resolve
              }
            },
            target: FetchInstruction.Resolve
          }
        }
        const notifications = notificationsResult.rows.map((row: any) => row.doc);
        await Promise.all(notifications.map((notification: Notification) =>
          Content.fetchProperties(notification, fetchInstructions, user)
        ));

        notifications.filter((notification: Notification) => notification.object && (notification.object as ASObject).type);
        const unreadNotificationsResult = await Notifications.getUnreadNotificationCount(user);
        const unreadItems = unreadNotificationsResult.rows.length ? unreadNotificationsResult.rows[0].value : 0;
        return {
          type: ObjectType.OrderedCollectionPage,
          totalItems: notificationsResult.total_rows,
          unreadItems,
          orderedItems: notifications,
        } as OrderedCollectionPage<any>;
      }
    });

    server.route({
      method: 'PATCH',
      path: '/api/notification',
      handler: async (request: Request) => {
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        const user: User = await Users.findUser(userId);
        const notification = request.payload;
        return await Notifications.update(user, notification);
      }
    });

    server.route({
      method: 'POST',
      path: '/api/notification',
      handler: async (request: Request) => {
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        const user: User = await Users.findUser(userId);
        await Notifications.markAllAsRead(user);
        return 'success';
      }
    });

    server.route({
      method: 'GET',
      path: '/api/activityOf',
      handler: async (request: Request) => {
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        const user: User = await Users.findUser(userId);
        const id = request.query.q;
        return Content.activityOf(user, id);
      }
    });

    server.route({
      method: 'GET',
      path: '/api/activityFrom',
      handler: async (request: Request) => {
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        const user: User = await Users.findUser(userId);
        const id = request.query.q;
        return Content.activityFrom(user, id);
      }
    });

    server.route({
      method: 'GET',
      path: '/api/relationship',
      handler: async (request: Request) => {
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        const user: User = await Users.findUser(userId);
        const actorId = request.query.actorId;
        return Content.relationship(user, actorId);
      }
    });

    server.route({
      method: 'GET',
      path: '/api/tags/{tag}',
      options: {
        validate: {
          query: Joi.object({
            index: Joi.number()
          }),
          params: Joi.object({
            tag: Joi.string().required()
          })
        }
      },
      handler: async (request: Request) => {
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        const user: User = await Users.findUser(userId);
        const tag = request.params.tag;
        const index = Number(request.query.index) || 0;
        const length = 10;
        const result = await Content.tags(user, tag, index, length);
        let next: string = undefined;
        if (result.rows && (length === result.rows.length)) {
          next = `${Config.app.baseUrl}/api/tags/${tag}?index=${index + length}`;
        }
        return {
          type: ObjectType.OrderedCollectionPage,
          next,
          // totalItems: result.total_rows, doesn't work on couchdb views
          orderedItems: result.rows.map((row: any) => row.doc),
        } as OrderedCollectionPage<ASObject>;
      }
    });

    /**
     * @api        {get} /api/amiloggedin Am I logged in
     * @apiName    Determine login status
     * @apiGroup   user
     * @apiDescription A simple API to determine if the user associated with this session is currently
     *                 logged in.
     *
     * @apiSuccess {json} loginStatus <code>true</code> is the user is logged in, otherwise <code>false</code>
     * @apiSuccessExample {json} LoggedIn:
     * "true"
     * @apiSuccessExample {json} NotLoggedIn:
     * "false"
     */
    server.route({
      method: 'GET',
      path: '/api/amiloggedin',
      options: {
        auth: {
          mode: 'optional'
        }
      },
      handler: (request: Request) => {
        return !!request.auth.credentials;
      }
    });

    /**
     * @api        {get} /api/myactor User Actor
     * @apiName    Get the logged-in user’s Actor object.
     * @apiGroup   user
     * @apiDescription An API for fetching the user’s actor object.
     *
     * @apiSuccess {json} Actor The Actor object corresponding to the current user.
     */
    server.route({
      method: 'GET',
      path: '/api/myactor',
      handler: async (request: Request) => {
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        const user: User = await Users.findUser(userId);
        return Content.getById(user, Content.getActorDocumentIdFromActorId(userId));
      }
    });

    server.state('userId', {
      ttl: null,
      isSecure: false,
      isHttpOnly: false,
      encoding: 'none',
    });

    server.state('actor', {
      ttl: null,
      isSecure: false,
      isHttpOnly: false,
      encoding: 'base64'
    })
  }
};
