'use strict';

const Joi = require('@hapi/joi');

const apContextValidation = Joi.string().allow('https://www.w3.org/ns/activitystreams');

module.exports = Object.freeze({
  userIdValidation: Joi.string().min(2).max(40).required(),
  passwordValidation: Joi.string().min(2).max(36).required(),
  emailValidation: Joi.string().email().required(),
  publishedValidation: Joi.string().isoDate(),
  contentValidation: Joi.string().min(1).max(280).required(),
  typeValidation: Joi.string().required(),
  idValidation: Joi.string().uri().required(),

  ref: Joi.alternatives().try(
    Joi.string().uri(),
    Joi.object({
      id: Joi.string().uri()
    }).unknown()
  ),

  recipients: Joi.array().items(Joi.string().uri()),
  contextValidation: Joi.alternatives().try(
      apContextValidation,
      Joi.array().items(
        Joi.alternatives().try(
          Joi.string().uri(),
          Joi.object()
        )
      ).has(apContextValidation)
    ),
});
