'use strict';
import {Request} from 'hapi';
import {User} from '../struct/User';
import {Job} from 'bullmq';
import {OrderedCollectionPage} from 'mammoth-activitystreams';
const { ObjectType } = require('mammoth-activitystreams');

const Users = require('../service/users');
const Queues = require('../service/queues');
const Boom = require('@hapi/boom');
const Config = require('config');

module.exports = {
  init: (server: any): void => {
    /**
     * @api        {get} /api/admin/queues Get the queues
     * @apiName    Queues
     * @apiGroup   admin
     *
     * @apiSuccess {Object} The queues
     */
    server.route({
      method: 'GET',
      path: '/api/admin/queues',
      handler: async (request: Request) => {
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        const user: User = await Users.findUser(userId);
        if (!user.admin) {
          throw Boom.forbidden('admin users only');
        }
        const queues = Queues.getQueues();
        return Object.keys(queues).map((queue: string) => Config.app.baseUrl + '/api/admin/queue/' + queue);
      }
    });

    /**
     * @api        {get} /api/admin/queues{queueName}/{type} Get a type of a queue
     * @apiName    Queue Types
     * @apiGroup   admin
     *
     * @apiSuccess {Object} The information on a queue
     */
    server.route({
      method: 'GET',
      path: '/api/admin/queue/{queueName}/{type}',
      handler: async (request: Request) => {
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        const user: User = await Users.findUser(userId);
        if (!user.admin) {
          throw Boom.forbidden('admin users only');
        }
        const queueName = request.params.queueName as string;
        const queueType = request.params.type as string;
        const index = Number.parseInt(request.query.index as string) || 0;
        const queues = Queues.getQueues();
        const queue = queues[queueName];
        let totalItems: number, orderedItems: Job[];
        let end: number;
        switch(queueType) {
          case 'active':
            totalItems = await queue.getActiveCount();
            end = Math.min(index + 10, totalItems);
            orderedItems = await queue.getActive(index, end);
            break;
          case 'waiting':
            totalItems = await queue.getWaitingCount();
            end = Math.min(index + 10, totalItems);
            orderedItems = await queue.getWaiting(index, end);
            break;
          case 'completed':
            totalItems = await queue.getCompletedCount();
            end = Math.min(index + 10, totalItems);
            orderedItems = await queue.getCompleted(index, end);
            break;
          case 'delayed':
            totalItems = await queue.getDelayedCount();
            end = Math.min(index + 10, totalItems);
            orderedItems = await queue.getDelayed(index, end);
            break;
          case 'failed':
            totalItems = await queue.getFailedCount();
            end = Math.min(index + 10, totalItems);
            orderedItems = await queue.getFailed(index, end);
            break;
          default:
            throw Boom.badRequest('Unknown queue type ' + queueType);
        }
        const id = Config.app.baseUrl + '/api/admin/queue/' + queueName + '/' + queueType;
        let next = undefined;
        if (end > totalItems) {
          next = id + '?index=' + (index + 10);
        }
        return {
          id: id,
          type: ObjectType.OrderedCollectionPage,
          next,
          totalItems,
          orderedItems
        } as OrderedCollectionPage<any>
      }
    })
  }
};
