'use strict';
/// <reference types="hapi" />

import {Request, ResponseToolkit} from 'hapi';
import {ClientConfiguration, ManagedUpload, PutObjectRequest} from 'aws-sdk/clients/s3';
import {Link} from 'mammoth-activitystreams';

const S3 = require('aws-sdk/clients/s3');

const Boom = require('@hapi/boom');
const Content = require('../db/content');
const Config = require('config');
const v4 = require('uuid').v4;
const _ = require('lodash');

const allowedContentTypes = [
  'image/jpeg',
  'image/gif',
  'image/png'
];

const extensions = {
  'image/jpeg': '.jpeg',
  'image/gif': '.gif',
  'image/png': '.png',
}

function checkContentType(contentType: string): void {
  if (!allowedContentTypes.includes(contentType)) {
    throw Boom.badData('content-type not allowed: ' + contentType);
  }
}

function createPutParams(file: any, contentType: string, userId: string): PutObjectRequest {
  // @ts-ignore
  const extension = extensions[contentType];
  return {
    Bucket: Config.s3.bucket,
    Key: v4() + extension,
    ACL: "public-read",
    Body: file,
    ContentType: contentType,
    Metadata: {
      // @ts-ignore
      userId: userId,
      actor: Content.getActorDocumentIdFromActorId(userId)
    }
  };
}

function createUploadOptions(): ManagedUpload.ManagedUploadOptions {
  return {
    partSize: 10 * 1024 * 1024,
    queueSize: 1
  };
}

function uploadToS3(s3: any, params: PutObjectRequest, options: ManagedUpload.ManagedUploadOptions): Promise<string> {
  return new Promise((resolve, reject) => {
    s3.upload(params, options, (err: Error, data: ManagedUpload.SendData) => {
      if (err) {
        console.error('error uploading to s3', err);
        reject(Boom.badGateway(err.message));
      } else {
        resolve(data.Location);
      }
    });
  });
}

function createS3Options(): ClientConfiguration {
  return _.cloneDeep(Config.s3.options);
}

module.exports = {
  init: (server: any): void => {
    server.route({
      method: 'POST',
      path: '/api/image',
      options: {
        payload: {
          allow: 'multipart/form-data',
          multipart: {
            output: 'stream',
          },
          parse: true,
          maxBytes: 12582912
        }
      },
      handler: async (request: Request, h: ResponseToolkit) => {
        // @ts-ignore
        const file = request.payload.file;
        const contentType = file.hapi.headers['content-type'];
        checkContentType(contentType);
        // @ts-ignore
        const userId = request.auth.credentials.userId;
        const putParams = createPutParams(file, contentType, userId);
        const s3 = new S3(createS3Options());
        const uploadOptions = createUploadOptions();

        const result: string = await uploadToS3(s3, putParams, uploadOptions);

        const link: Link = {
          type: 'Link',
          href: result,
          mediaType: contentType
        }

        return h.response(JSON.stringify(link))
          .type('application/activity+json')
          .header('Location', result)
          .code(201);
      }
    });
  }
};
