'use strict';

const JoiValidations = require('./joi-validations');

describe('ref tests', () => {
  const Joi = require('@hapi/joi');
  test('string ref works', () => {
    expect(() => Joi.assert("http://me.here/foo", JoiValidations.ref)).not.toThrow();
  });

  test('string ref fails if not uri', () => {
    expect(() => Joi.assert("hello, world!", JoiValidations.ref)).toThrow();
  });

  test('string object works', () => {
    expect(() => Joi.assert({ id: "http://hello.world/" }, JoiValidations.ref)).not.toThrow();
  });

  test('string object with extras works', () => {
    expect(() => Joi.assert({ id: "http://hello.world/", summary: "here's a hello world" }, JoiValidations.ref)).not.toThrow();
  });

  test('string object works', () => {
    expect(() => Joi.assert({ id: "hello, world!" }, JoiValidations.ref)).toThrow();
  });

  test('nothing at all doesn\'t work', () => {
    expect(() => Joi.assert(null, JoiValidations.ref)).toThrow();
  });

  test('context validation works', () => {
    expect(() => Joi.assert('https://www.w3.org/ns/activitystreams', JoiValidations.contextValidation)).not.toThrow();
  });

  test('context validation works in an array', () => {
    expect(() => Joi.assert(['https://www.w3.org/ns/activitystreams', 'https://foo.bar.org/'], JoiValidations.contextValidation)).not.toThrow();
  });

  test ('context validation supports arrays with objects', () => {
    const context = [
        "https://www.w3.org/ns/activitystreams",
        "https://search.fedi.app/schemas/litepub-0.1.jsonld",
        {
          "@language": "und"
        }
      ];
    expect(() => Joi.assert(context, JoiValidations.contextValidation)).not.toThrow();
  });
});
