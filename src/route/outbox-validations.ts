'use strict';

import {ASObject, Create, ObjectRefs, objectRefsType} from 'mammoth-activitystreams';
import {User} from '../struct/User';

const Users = require('../service/users');
const Boom = require('@hapi/boom');


function validateAttributedTo(attributedTo: ObjectRefs<ASObject>, user: User) {
  if (attributedTo !== Users.getActorDocumentIdFromActorId(user.userId)) {
    throw Boom.unauthorized('user not authorized to send message attributed to ' + attributedTo, user.userId);
  }
}

function validateContent(content: string | undefined) {
  if (!content || content.length === 0) {
    throw Boom.badRequest('content may not be missing or blank');
  }
}

function validateObject(objectRefs: ObjectRefs<ASObject>, user: User) {
  if (['ASObject', 'Mention'].includes(objectRefsType(objectRefs))) {
    throw Boom.badRequest('activity object must be embedded in the create message and may not be linked', objectRefs);
  }
  const object = objectRefs as ASObject;
  validateAttributedTo(object.attributedTo, user);
  validateContent(object.content);
}

module.exports = {
  validateOutboxSendRequest: (payload: Create, user: User) => {
    validateObject(payload.object, user);
  }
};
