'use strict';
/// <reference types="hapi" />


import {Request, ResponseToolkit} from 'hapi';
import {User} from '../struct/User';
import {Activity, ASObject, ObjectRefs, OrderedCollectionPage} from 'mammoth-activitystreams';
const { ObjectType } = require('mammoth-activitystreams');
import {Actor, APObject} from 'mammoth-activitypub';
import {FetchInstructions} from 'mammoth-api';

const Boom = require('@hapi/boom');
const Content = require('../db/content');
const ActivityStreamsHandler = require('../service/activity-dispatcher');
const Users = require('../service/users');
const HttpSignature = require('http-signature');
const crypto = require('crypto');

async function verifyHttpSignatures(user: User, request: Request): Promise<Actor> {
  let parsed;
  try {
    parsed = HttpSignature.parseRequest(request.raw.req, {
      authorizationHeaderName: 'Signature',
      clockSkew: 300
    });
  } catch (e) {
    console.warn('unable to parse http-signature', e);
    throw Boom.unauthorized(e.message);
  }
  let actor: Actor;
  let actorId = parsed.params.id;
  if (!actorId) {
    actorId = parsed.params.keyId.substr(0, parsed.params.keyId.indexOf('#'));
  }
  if (!actorId) {
    console.info('http-signature missing id; key ' + parsed.params.keyId);
    throw Boom.unauthorized('http-signature missing id field; unable to verify signature');
  }
  try {
    actor = await Content.resolveActor(actorId, user);
  } catch (e) {
    throw Boom.unauthorized('cannot fetch actor’s public key: ' + e.message);
  }
  if (!actor) {
    throw Boom.unauthorized('unable to find actor for id: ' + actorId);
  }
  if (actor.publicKey.id !== parsed.params.keyId) {
    throw Boom.unauthorized(`Actor's key id (${actor.publicKey.id}) doesn't match key passed in authorization header (${parsed.params.keyId})`);
  }
  const publicKey = actor.publicKey.publicKeyPem;
  if (!HttpSignature.verifySignature(parsed, publicKey)) {
    console.warn('signature verification failed: ' + JSON.stringify(parsed));
    console.debug('request: ' + JSON.stringify(request.raw.req));
    throw Boom.unauthorized('unable to verify authorization signature against user’s public key');
  }
  return actor;
}

// maps the name of an algorithm specified by the Digest RFC (RFC3230, RFC 5843) to the corresponding algorithm in crypto.
// we do not accept UnixSum as a valid algorithm
const digestAlgorithmToCryptoAlgorithm: { [index: string]: string } = {
  'SHA-256': 'sha256',
  'SHA-512': 'sha512',
  'SHA': 'sha1',
  'MD5': 'md5'
};

module.exports = {
  init: (server: any) => {

    server.ext('onRequest', (request: Request, h: ResponseToolkit) => {
      if (request.headers.digest) {
        const digestHeaders = request.headers.digest.split(',');
        const includedDigests: { [index: string]: string } = {};
        const computedDigests: { [index: string]: any } = {};
        digestHeaders.forEach((digestive: string) => {
          const splitter = digestive.indexOf('=');
          const algorithm = digestive.substring(0, splitter);
          includedDigests[algorithm] = digestive.substring(splitter + 1);
          const cryptoAlgorithm = digestAlgorithmToCryptoAlgorithm[algorithm];
          if (!cryptoAlgorithm) {
            throw Boom.badRequest('Digest algorithm not supported', algorithm);
          }
          computedDigests[algorithm] = crypto.createHash(cryptoAlgorithm);
        });

        request.events.on('peek', (chunk) => {
          for (const digester of Object.values(computedDigests)) {
            digester.update(chunk);
          }
        });

        request.events.once('finish', () => {
          for (const [algorithm, digester] of Object.entries(computedDigests)) {
            const digest = digester.digest('base64');
            if (digest !== includedDigests[algorithm]) {
              throw Boom.badRequest('digest does not match body: ' + algorithm);
            }
          }
        });

      }
      return h.continue;
    });

    /**
     * @api        {post} /a/{actor}/inbox Inbox Receive Activity
     * @apiName    Receive an S2S ActivityPub request
     * @apiGroup   S2S
     * @apiDescription  This is the only API for other servers to send an ActivityPub
     *             object. Requests must be signed with http-signature corresponding
     *             to the Actor mentioned in the activity.
     *
     * @apiParam {String} user The name of a user of the system
     * @apiParam {JSON} body An ActivityPub Activity object.
     * @apiParam {URL} body.id An ID is required and must be resolvable.
     * @apiParam {String} body.type Must be a valid ActivityPub activity type.
     * @apiParam {String} body.actor must correspond to the Actor specified in the http-signature
     * @apiParamExample {json} Request-Example:
     *     {
     *       "id": "https://www.example.com/12345",
     *       "type": "Like",
     *       "object": "https://www.example.com/98765",
     *       "actor": "https://www.example.com/a/bob"
     *     }
     *
     * @apiSuccess (Success 201) {String} response <code>document accepted</code>
     * @apiSuccessExample {http} Success-Response:
     *     HTTP/2 201 Accepted
     *     Content-Type: text/text
     *     Location: https://example.com/a/bob/12345
     *
     *     document accepted
     *
     * @apiError (Error 404) UserNotFound the <code>user</code> does not exist on this platform
     * @apiError (Error 401) HttpSignatureInvalid the http-signature is not in a valid format
     * @apiError (Error 401) HttpSignatureNotFound the request had no http-signature
     * @apiError (Error 401) HttpSignatureMissingIdField the http-signature is missing the id of the actor
     * @apiError (Error 401) HttpSignatureNotParseable the http-signature could not be parsed
     * @apiError (Error 401) HttpSignatureActorNotFetchable the actor corresponding to the http-signature could not be retrieved
     * @apiError (Error 401) HttpSignatureActorKeyDoesntMatchHttpSignatureKey the Actor's public key doesn't match the key used in the http-signature
     * @apiError (Error 401) HttpSignatureDoesNotVerify the signature generated doesn't match the http-signature
     * @apiError (Error 403) ActorForbidden The Actor is not allowed to send this Activity as it belongs to another Actor
     * @apiError (Error 400) UnhandledActivity Server unable to handle activities of this type.
     */
    // Incoming messages from the public, only.
    server.route({
      method: 'POST',
      path: '/a/{actor}/inbox',
      options: {
        auth: false
      },
      handler: async (request: Request, h: ResponseToolkit) => {
        let user: User;
        try {
          user = await Users.findUser(request.params.actor);
        } catch (e) {
          if (e.statusCode === 404) {
            throw Boom.notFound('user not found: ' + request.params.user);
          }
          throw e;
        }

        if (!request.headers.signature) {
          throw Boom.unauthorized('signature header (http-signature) required');
        }
        const actor = await verifyHttpSignatures(user, request);

        const doc = request.payload as Activity;
        try {
          await ActivityStreamsHandler.handlePostedDocument(user, actor, doc);
          return h.response('document accepted')
            .type('text/text')
            .header('Location', doc.id)
            .code(201);
        } catch (e) {
          if (Boom.isBoom(e)) {
            throw e;
          }
          throw Boom.badImplementation(e);
        }
      }
    });
    const inboxPageSize = 4;
    /**
     * @api        {get} /a/{actor}/inbox Get Inbox Collection
     * @apiName    Get timeline
     * @apiGroup   C2S
     * @apiDescription This implements the C2S interface for getting a user's inbox, which is an
     *                 ordered, paged collection representing their timeline. It's sorted in reverse
     *                 chronological order of the published date of the items it contains. The top-level
     *                 items in the timeline will be Document subclasses without an
     *                 <code>inReplyTo</code> field.
     *
     * @apiParam {json} fetchInstructions The fields of the object to be fetched into concrete objects
     *           rather than object identifiers.
     * @apiParamExample {json} fetchInstructions-example:
     * {
     *   "fetch": {
     *     "attributedTo": "Resolve",
     *     "actor": "Resolve",
     *     "object": {
     *       "type": "Object",
     *       "fetch": {
     *         "attributedTo": "Resolve",
     *         "actor": "Resolve"
     *       }
     *     },
     *     "likes": "PopulateCollection",
     *     "shares": "PopulateCollection",
     *     "replies": {
     *       "type": "Collection",
     *       "recursive": true
     *     }
     *   }
     * }
     *
     * @apiSuccess {Object} OrderedCollectionPage An page of Documents and with the corresponding fields fetched.
     * @apiSuccessExample {json} Inbox-example:
     * {
     *   "type": "OrderedCollectionPage",
     *   "totalItems": 42,
     *   "orderedItems": [
     *     {
     *       "id": "https://mammoth.example.com/a/bob/12345",
     *       "type": "Note",
     *       "published": "2020-06-01T12:34:56Z",
     *       "content": "Hello, world!",
     *       "attributedTo": {
     *         "id": "https://mammoth.example.com/a/bob",
     *         "preferredName": "Bob!"
     *         "type": "Person"
     *       }
     *     },
     *     ...
     *   ]
     * }
     */
    // a list of all stuff (?) for the current user.
    server.route({
      method: 'GET',
      path: '/a/{actor}/inbox',
      handler: async (request: Request) => {
        const requestedUser = await Users.findUser(request.params.actor);
        const authenticatedUser: User = request.auth.credentials as User;
        if (authenticatedUser && authenticatedUser.userId === request.params.actor) {
          let timeline;
          const currentOffset: number = Number(request.query.offset) || 0;
          try {
            timeline = await Content.timeline(requestedUser, inboxPageSize, currentOffset);
          } catch (e) {
            throw e;
          }
          const items = [] as ASObject[];
          const promises: Promise<any>[] = [];
          const fetchInstructions: FetchInstructions = request.query.fetchInstructions ?
            JSON.parse(request.query.fetchInstructions as string) as FetchInstructions
            : null;
          timeline.rows.forEach((row: any) => {
            const object: APObject = row.doc;
            if (fetchInstructions) {
              promises.push(Content.fetchProperties(object, fetchInstructions, requestedUser));
            }
            items.push(object);
          });
          try {
            await Promise.all(promises);
          } catch (e) {
            console.error(e);
            throw e;
          }
          let nextPage = undefined;
          if (timeline.total_rows > currentOffset + items.length) {
            nextPage = Content.getActorDocumentIdFromActorId(requestedUser.userId) +
              '/inbox?offset=' + (currentOffset + items.length);
          }
          const inbox: OrderedCollectionPage<ASObject> = {
            type: ObjectType.OrderedCollectionPage,
            totalItems: timeline.total_rows,
            orderedItems: items as ObjectRefs<ASObject>
          };
          if (nextPage) {
            inbox.next = nextPage;
          }
          return inbox;
        }

        if (requestedUser) {
          return Boom.badRequest('no access to inbox of ' + request.params.actor);
        }

        Boom.notFound('User ' + request.params.actor + ' not found');
      }
    });
  }
};
