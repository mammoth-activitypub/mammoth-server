'use strict';
/// <reference types="jest" />
/// <reference types="rewire" />

import {ServerInjectOptions} from 'hapi';
import {User} from '../struct/User';

const Hapi = require('@hapi/hapi');
const rewire = require('rewire');

let UserRoutes = rewire('../../dist/route/user-routes');

let server: any;
let Users: any;
let Content: any;
let createUserMock = jest.fn();
let authenticateMock = jest.fn();
let getByIdMock = jest.fn();

beforeEach(async () => {
  UserRoutes = rewire('../../dist/route/user-routes');
  server = Hapi.server();
  await server.register(require('@hapi/cookie'));

  server.auth.strategy('session', 'cookie', {
    cookie: {
      password: 'superkalifragilisticexpealidocious', // cookie secret
      isSecure: false,
      name: 'mammoth-auth', // Cookie name
      ttl: 24 * 60 * 60 * 1000 // Set session to 1 day
    },
    validateFunc: () => {
      return {valid: true, credentials: { userId: 'bob' }}
    }
  });
  server.auth.default('session');
  server.validator(require('@hapi/joi'));

  createUserMock = jest.fn();
  authenticateMock = jest.fn();
  getByIdMock = jest.fn();
  Users = {
    createUser: createUserMock,
    authenticate: authenticateMock,
    getActorDocumentIdFromActorId: (userId: string) => { return `http://someurl/${userId}`}
  };
  Content = {
    getById: getByIdMock
  };
  UserRoutes.__set__('Users', Users);
  UserRoutes.__set__('Content', Content);
  UserRoutes.init(server);
});

describe('create user', () => {
  const goodCreateUserRequest:ServerInjectOptions = {
    url: '/api/user',
    method: 'POST',
    payload: {
      userId: 'uid',
      password: 'pw',
      email: 'email@here.org'
    }
  };
  test('can create a user if everything is present', async () => {
    createUserMock.mockResolvedValueOnce({userId: 'bob'});
    const result = await server.inject(goodCreateUserRequest);
    expect(createUserMock.mock.calls.length).toBe(1);
    expect(result.statusCode).toBe(200);
  });

  test('cannot create a user if said user already exists', async () => {
    createUserMock.mockRejectedValue({ message: 'user already exists', statusCode: 409});
    const result = await server.inject(goodCreateUserRequest);
    expect(result.statusCode).toBe(409);
  });

  test.skip('cannot create a user if email is missing', async () => {
    const serverRoute: ServerInjectOptions = {
      url: '/api/user',
      method: 'POST',
      payload: {
        userId: 'uid',
        password: 'pw'
      }
    };
    expect((await server.inject(serverRoute)).statusCode).toBe(400);
  });
});

describe('login tests', () => {
  const loginRequest: ServerInjectOptions = {
    url: '/api/login',
    method: 'POST',
    payload: {
      userId: 'uid',
      password: 'password'
    }
  };
  const user: Partial<User> = {
    userId: 'uid',
    password: 'foo',
    hashedPassword: 'hashedPassword',
    userDb: 'uid',
    email: 'me@here.org'
  };
  const actor: any = {
    userId: 'uid'
  };

  test('successful login succeeds successfully', async () => {
    authenticateMock.mockResolvedValue(user);
    getByIdMock.mockResolvedValue(actor);
    const result = await server.inject(loginRequest);
    expect(result.statusCode).toBe(200);
    expect(result.result).toEqual(actor);
    expect(result.headers['set-cookie'][0]).toMatch(/^mammoth-auth=/)
  });

  test.skip('unsuccessful login fails', async () => {
    authenticateMock.mockResolvedValue(null);
    const result = await server.inject(loginRequest);
    expect(result.statusCode).toBe(401);
  });
});
