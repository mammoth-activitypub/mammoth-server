'use strict';

import {Job, JobsOptions, Processor } from 'bullmq';
const {Queue, QueueScheduler, Worker} = require('bullmq');

const Config = require('config');

const queues: { [index: string]: any } = { };
const queueSchedulers: Record<string, any> = {};

const connection = Config.redis ? Config.redis.connection : undefined;

module.exports = {
  registerQueue: (queueName: string, processor: Processor, concurrency: number, defaultJobOptions? : JobsOptions): void => {
    if (queues[queueName]) {
      throw Error(`queue already registered: ${queueName}`);
    }
    queues[queueName] = new Queue(queueName, {
      connection,
      defaultJobOptions
    });
    queueSchedulers[queueName] = new QueueScheduler(queueName, {
      connection
    });
    new Worker(queueName, processor, { connection, concurrency: 3 });
  },
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  addItem: (queueName: string, name: string, item: any): Promise<Job<any, any>> => {
    const queue = queues[queueName];
    if (!queue) {
      throw(Error(`no queue defined for ${queueName}`));
    }
    console.info(`added item to queue ${queueName}: ${name}`);
    return queue.add(name, item, Config.app.workQueue.conf);
  },
  getQueues: (): Record<string, any> => {
    return Object.freeze(queues);
  }
}
;
