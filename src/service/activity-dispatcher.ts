'use strict';
/// reference types="axios"

import {User} from '../struct/User';
import {
  Accept,
  Activity,
  Actor as ASActor,
  ActorRefs,
  Announce,
  ASObject,
  Collection,
  Create,
  Delete,
  Dislike,
  Follow,
  Like,
  ObjectRefs,
  Offer,
  Reject,
  TentativeAccept,
  TentativeReject,
  Undo,
  Update
} from 'mammoth-activitystreams';
const { ObjectType, objectRefType, resolveActorRefs, resolveObjectRef, resolveObjectRefs } = require('mammoth-activitystreams');
import type {Actor, APObject} from 'mammoth-activitypub';
const { sendActivity } = require('./activity-transport');

export {};

const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');
let Content = require('../db/content');
const JoiValidations = require('../route/joi-validations');
const Users = require('./users');
require('./activity-transport');
const v4 = require('uuid').v4;
const sanitizeHtml = require('sanitize-html');
const Notifications = require('../db/Notifications');

const creatableAndDeletableTypes = [
  ObjectType.Document,
  ObjectType.Page,
  ObjectType.Note,
  ObjectType.Article,
  ObjectType.Audio,
  ObjectType.Video,
  ObjectType.Event
];

const createActivityValidation = Joi.object({
  // "@context": JoiValidations.contextValidation.required(),
  id: Joi.string().uri().required(),
  actor: JoiValidations.ref.required(),
  object: Joi.object({
    id: Joi.string().uri().required(),
    type: Joi.string().valid(...creatableAndDeletableTypes).required(),
    attributedTo: JoiValidations.ref.required(),
    content: Joi.string().required()
  }).unknown().required(),
  to: JoiValidations.recipients,
  bto: JoiValidations.recipients,
  cc: JoiValidations.recipients,
  bcc: JoiValidations.recipients,
  audience: JoiValidations.recipients
}).or('to', 'bto', 'cc', 'bcc', 'audience')
  .unknown();

async function handleCreateActivity(user: User, actor: Actor, activity: Create): Promise<void> {
  const object = activity.object as APObject; // create activity should always have the object embedded in the activity
  // Joi.assert(activity, createActivityValidation);
  const objectRef = resolveObjectRef(object);
  const localCopy = await Content.getById(user, objectRef);
  if (localCopy) {
    throw Boom.badRequest('Object ' + objectRef + ' already exists');
  }

  await Content.save(user, activity.id, activity);

  object.tag = getHashtags(object.content);
  await Content.save(user, object.id, object);
}

function getHashtags(contentHtml: string): ObjectRefs<ASObject> {
  const content: string = sanitizeHtml(contentHtml, { allowedTags: [] });
  const hashtags = content.match(/(?:^|\B)#(?![\p{Nd}\p{Pc}]+\b)([\p{L}\p{Nl}\p{Nd}\p{Pc}]{1,30})(?:\b|\r)?/gu);
  if (hashtags) {
    return hashtags.map((tag: string) => {
      return {
        type: 'Hashtag',
        name: tag.trim()
      }
    }) as unknown as ObjectRefs<ASObject>;
  }
  return undefined;
}

function configureUserSubmittedDelete(user: User, activity: Delete): void {
  activity.published = new Date().toISOString();
  activity.updated = activity.published;
  activity.deleted = activity.published;
}

const deleteActivityValidation = Joi.object({
  id: Joi.string().uri().required(),
  actor: JoiValidations.ref.required(),
  object: JoiValidations.ref.required()
}).unknown();

async function handleDeleteActivity(user: User, activity: Delete): Promise<void> {
  Joi.assert(activity, deleteActivityValidation);
  if (user) {
    configureUserSubmittedDelete(user, activity);
  }
  await Content.save(user, activity.id, activity);
}

function needsActivityWrapper(doc: ASObject, userActorId: string): boolean {
  if (userActorId === resolveObjectRefs(doc.attributedTo)) {
    return creatableAndDeletableTypes.includes(doc.type);
  }
}

const reactionActivityValidation = Joi.object({
  id: Joi.string().uri().required(),
  actor: JoiValidations.ref.required(),
  object: JoiValidations.ref.required()
}).unknown();

/**
 * To handle followers/following collections, we need to have the object embedded into the activity.
 *
 * @param user
 * @param actor
 * @param activity
 */
async function handleEmbeddedResponseObjectActivity(user: User, actor: Actor,
    activity: Accept | TentativeAccept | Reject | TentativeReject): Promise<void> {
  // security:
  // don't assume that an already-embedded object is legitimate; fetch it from a trusted source
  const activityObject = await Content.resolveObject(activity.object, null, user) as Activity;
  if (!activityObject) {
    throw Boom.badImplementation('somehow we couldn\'t resolve object ' + resolveObjectRef(activity.object));
  }
  const objectTarget = await Content.resolveObject(activityObject.target);
  activity.object = activityObject;
  if (!await Content.rev(user, activity.id)) {
    await Content.save(user, activity.id, activity);
  }
  if (!await Content.rev(user, activityObject.id)) {
    await Content.save(user, activityObject.id, activityObject);
  }
  if (objectTarget && !await Content.rev(user, objectTarget.id)) {
    await Content.save(user, objectTarget.id, objectTarget);
  }
}

async function handleObjectActivity(user: User, actor: Actor,
    activity: Like | Dislike | Undo | Announce | TentativeAccept | Reject | TentativeReject): Promise<void> {
  Joi.assert(activity, reactionActivityValidation);

  await Content.save(user, activity.id, activity);
}

const undoActivityValidation = Joi.object({
  actor: JoiValidations.ref.required(),
  object: JoiValidations.ref.required()
}).unknown();

async function handleUndoActivity(user: User, actor: Actor, activity: Undo): Promise<void> {
  Joi.assert(activity, undoActivityValidation);
  const objectRef = resolveObjectRef(activity.object);
  const object = await Content.getById(user, objectRef) as Activity;
  activity.object = object;
  if (!object) {
    throw Boom.notFound('referenced object not found: ' + objectRef);
  }
  if (![ObjectType.Like, ObjectType.Dislike, ObjectType.Follow].includes(object.type)) {
    throw Boom.badRequest('Undo not supported for activities of type' + object.type);
  }
  if (resolveActorRefs(activity.actor) !== resolveActorRefs(object.actor)) {
    throw Boom.badRequest('Actor not allowed to delete this object', resolveActorRefs(activity.actor));
  }
  await Content.deleteDocument(user, objectRef, (object as any)._rev);
}

const followActivityValidation = Joi.object({
  '@context': JoiValidations.contextValidation,
  id: Joi.string().uri().required(),
  actor: JoiValidations.ref.required(),
  object: JoiValidations.ref.required()
}).unknown();

async function handleFollowActivity(user: User, actor: Actor, followActivity: Follow): Promise<any> {
  const ownerId = Users.getActorDocumentIdFromActorId(user.userId);
  const owner: boolean = isOwner(user, actor);
  if (owner) {
    followActivity.id = ownerId + '/' + v4();
  }
  try {
    Joi.assert(followActivity, followActivityValidation);
  } catch (e) {
    throw Boom.boomify(e, {statusCode: 400});
  }
  const actorId = resolveActorRefs(followActivity.actor) as ObjectRefs<ASActor | Collection<ASObject>>;
  const objectRef = resolveObjectRef(followActivity.object);
  if (!owner && objectRef !== ownerId) {
    throw Boom.badRequest('object must be ' + ownerId);
  }
  if (owner && actorId !== ownerId) {
    throw Boom.badRequest('user cannot self-follow');
  }
  await Content.save(user, followActivity.id, followActivity);

  if (!owner) {
    const accept: Accept = {
      '@context': 'https://www.w3.org/ns/activitystreams',
      id: ownerId + '/' + v4(),
      type: ObjectType.Accept,
      to: actorId,
      actor: ownerId,
      object: followActivity.id
    };

    await Content.save(user, accept.id, accept);

    return sendActivity(user.userId, accept);
  }
}

async function handleEmbeddedNewObjectActivity(user: User, actor: Actor, offer: Offer): Promise<any> {
  const ownerId = Users.getActorDocumentIdFromActorId(user.userId);
  const owner: boolean = isOwner(user, actor);
  const object = await Content.resolveObject(offer.object) as ASObject;
  if (owner) {
    offer.id = ownerId + '/' + v4();
    if (objectRefType(offer.object) === 'ASObject') {
      if (!object.id) {
        object.id = ownerId + '/' + v4();
      }
    }
  }
  try {
    Joi.assert(offer, followActivityValidation);
  } catch (e) {
    throw Boom.boomify(e, {statusCode: 400});
  }
  const actorId = resolveActorRefs(offer.actor) as ObjectRefs<ASActor | Collection<ASObject>>;
  const objectRef = resolveObjectRef(offer.object);
  if (!owner && objectRef !== ownerId) {
    throw Boom.badRequest('object must be ' + ownerId);
  }
  await Content.save(user, offer.id, offer);
}

async function handleUpdateUserActivity(user: User, actor: Actor, update: Update): Promise<void> {
  const ownerId = Content.getActorDocumentIdFromActorId(user.userId);
  update.id = ownerId + '/' + v4();
  const object = update.object as ASObject;
  const original = await Content.getById(user, object.id);
  if (!original.attributedTo === ownerId) {
    throw Boom.forbidden('Not allowed to update someone else’s object: ' + object.id);
  }
  for (let [key, value] of Object.entries(object)) {
    original[key] = value;
  }
  original.updated = new Date().toISOString();
  update.object = original;
  original.tag = getHashtags(original.content);

  await Content.save(user, original.id, original, original._rev);
}

async function handleUpdateServerActivity(user: User, actor: Actor, update: Update): Promise<void> {
  if (objectRefType(update.object) !== 'ASObject') {
    throw Boom.badRequest('activity.object must be included by value, not by reference');
  }
  const object = update.object as ASObject;
  if (!creatableAndDeletableTypes.includes(object.type)) {
    throw Boom.forbidden('Not allowed to Update objects of this type: ' + object.type);
  }
  const actorId = resolveObjectRef(actor);
  if (resolveObjectRef(update.actor) !== actorId ||
      resolveObjectRefs(object.attributedTo) !== actorId) {
    throw Boom.forbidden('Not allowed to update someone else’s object: ' + object.id);
  }
  const original = await Content.getById(user, resolveObjectRef(object));
  if (original && resolveObjectRefs(original.attributedTo) !== resolveObjectRefs(object.attributedTo)) {
    throw Boom.forbidden('Not allowed to change attribution of an object: ' + object.id);
  }
  object.tag = getHashtags(object.content);
  // todo check object for validity
  await Content.save(user, object.id, object, original ? original._rev : undefined);
}

async function handleUpdateActivity(user: User, actor: Actor, update: Update): Promise<void> {
  if (isOwner(user, actor)) {
    await handleUpdateUserActivity(user, actor, update);
  } else {
    await handleUpdateServerActivity(user, actor, update);
  }
  await Content.save(user, update.id, update);
}

async function dispatchNotification(user: User, activity: Activity): Promise<void> {
  const notificationType: string = await Notifications.shouldCreateNotification(user, activity);
  if (notificationType) {
    await Notifications.createNotification(user, activity, notificationType);
  }
}

async function dispatchActivity(user: User, actor: Actor, activity: Activity): Promise<void> {
  switch (activity.type) {
    case ObjectType.Create:
      await handleCreateActivity(user, actor, activity as Create);
      break;

    case ObjectType.Delete:
      await handleDeleteActivity(user, activity as Delete);
      break;

    case ObjectType.TentativeAccept:
    case ObjectType.Reject:
    case ObjectType.TentativeReject:
    case ObjectType.Accept:
      await handleEmbeddedResponseObjectActivity(user, actor, activity as Accept);
      break;

    case ObjectType.Like:
    case ObjectType.Announce:
    case ObjectType.Dislike:
      await handleObjectActivity(user, actor, activity as Like);
      break;

    case ObjectType.Undo:
      await handleUndoActivity(user, actor, activity as Undo);
      break;

    case ObjectType.Follow:
      await handleFollowActivity(user, actor, activity as Follow);
      break;

    case ObjectType.Update:
      await handleUpdateActivity(user, actor, activity as Update);
      break;

    case ObjectType.Offer:
    case ObjectType.Invite:
      await handleEmbeddedNewObjectActivity(user, actor,activity as Offer);
      break;

    default:
      throw Boom.boomify(new Error('unable to handle activities of type ' + activity.type), {statusCode: 400});
  }
  if (isOwner(user, actor)) {
    console.log('queueing send-activity');
    await sendActivity(user.userId, activity);
  } else {
    await dispatchNotification(user, activity);
  }
}

function wrapInCreateActivity(actorId: string, doc: ASObject): Create {
  delete doc['@context'];
  return {
    '@context': 'https://www.w3.org/ns/activitystreams',
    type: ObjectType.Create,
    id: actorId + '/' + v4(),
    actor: resolveActorRefs(doc.attributedTo as ActorRefs) as string,
    object: doc,
    published: doc.published,
    to: doc.to,
    cc: doc.cc,
    bto: doc.bto,
    bcc: doc.bcc,
    audience: doc.audience
  };
}

function isOwner(user: User, actor: Actor): boolean {
  return Users.getActorDocumentIdFromActorId(user.userId) === actor.id;
}

// make sure that the actor, origin, target, and object are present in
// either the activity or in the user's database, if possible.
function fetchActivityObjects(user: User, activity: Activity): Promise<any> {
  const promises: Promise<any>[] = [];
  ['actor', 'object', 'target', 'origin'].forEach((i) => {
    // @ts-ignore
    const collection = activity[i];
    if (collection) {
      try {
        promises.push(Content.resolveObject(collection, null, user));
      } catch (e) {
        console.info('unable to fetch ' + i + ' of activity ' + activity.id, e.message);
      }
    }
  });
  return Promise.all(promises);
}

function sanitize (html: string): string {
  return sanitizeHtml(html, {
    allowedTags: ['blockquote', 'p', 'ul', 'ol',
      'nl', 'li', 'b', 'i', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'pre']
  });
}

async function confirmActorHasPermissionToSendDocument(user: User, actor: Actor, activity: Activity): Promise<void> {
  if (!activity.actor) {
    throw Boom.forbidden('Activity has no actor');
  }
  const activityActorId = resolveActorRefs(activity.actor);
  if (actor.id !== activityActorId) {
    if (activity.inReplyTo) {
      // Friendica forwards messages to recipients because they don't believe in scale
      const repliedMessage = await Content.resolveObject(activity.inReplyTo, null, user);
      if (repliedMessage) {
        const attributedToActor = resolveActorRefs(repliedMessage.attributedTo);
        if (attributedToActor === activityActorId) {
          return;
        }
      }
    }
    if (activity.object) {
      // friendica forwards likes too
      const activityObject = await Content.resolveObject(activity.object);
      if (activityObject?.attributedTo) {
        const objectActor = resolveActorRefs(activityObject.attributedTo);
        if (objectActor) {
          if (objectActor === activityActorId) {
            return;
          }
        }
      }
    }
    // throw Boom.forbidden('actor ' + actor.id + ' may not submit activities for actor ' + activityActorId);
    //
    // // confirm that the object identifier corresponds to the actor
    // const actorHost = new URL(actor.id).hostname;
    // const activityHost = new URL(activity.id).hostname;
    // if (actorHost !== activityHost) {
    //   throw Boom.badRequest('actor may not submit activities with IDs from another host');
    // }
  }
}

exports.handlePostedDocument = async (user: User, actor: Actor, doc: ASObject): Promise<Activity> => {
  console.info('processing activity', doc.id);
  try {
    if (isOwner(user, actor)) {
      const userActorId = Users.getActorDocumentIdFromActorId(user.userId);
      doc.id = userActorId + '/' + v4();
      doc.published = new Date().toISOString();
      doc.updated = doc.published;
      if (doc.content) {
        doc.content = sanitize(doc.content);
      }
      if (needsActivityWrapper(doc, userActorId)) {
        // wrap Object in Create Activity
        doc = wrapInCreateActivity(userActorId, doc);
      }
    }
    const activity = doc as Activity;

    await confirmActorHasPermissionToSendDocument(user, actor, activity);

    await fetchActivityObjects(user, activity);
    await dispatchActivity(user, actor, activity);

    console.info('successfully processed activity', activity.id);
    return activity;
  } catch (e) {
    console.info(`failed to process activity ${doc.id}: ${e.message}`);
    throw e;
  }
};

