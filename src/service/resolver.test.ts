'use strict';
/// <reference types="jest" />
/// <reference types="rewire" />

import {ASObject, CollectionPageRef, Link, ObjectRefs, ObjectType} from 'mammoth-activitystreams';
import {Actor} from 'mammoth-activitypub';
import each from 'jest-each';

const rewire = require('rewire');
let Resolver: any;

beforeEach(() => {
  Resolver = rewire('../../dist/service/resolver');
});


describe.skip('resolveLinkRef tests', () => {
  test('it correctly resolves strings', () => {
    expect(Resolver.resolveLinkRef('http://foo')).toBe('http://foo');
  });

  test('it correctly resolves Links', () => {
    const link: Link = {
      type: "Link",
      href: "http://foo"
    };
    expect(Resolver.resolveLinkRef(link)).toBe('http://foo');
  });
});

describe.skip('resolveObjectRef tests', () => {
  test('it resolves strings correctly', () => {
    expect(Resolver.resolveObjectRef("foo")).toBe("foo");
  });

  test('it resolves links correctly', () => {
    const link: Link = {
      type: "Link",
      href: "foo"
    };
    expect(Resolver.resolveObjectRef(link)).toBe("foo");
  });

  test('it resolves Objects correctly', () => {
    const object: Partial<ASObject> = {
      id: "foo"
    };
    expect(Resolver.resolveObjectRef(object)).toBe("foo");
  });
});

describe.skip('resolveObjectRefs', () => {
  test('it works with an array', () => {
    const objectRefs = ['ab', 'bc', 'cd'] as unknown as ObjectRefs<any>;
    expect(Resolver.resolveObjectRefs(objectRefs)).toEqual(['ab', 'bc', 'cd']);
  });

  test('it works with a single ref', () => {
    expect(Resolver.resolveObjectRefs('abc')).toBe('abc');
  });
});

describe.skip('resolveActorRef', () => {
  test('it handles strings', () => {
    expect(Resolver.resolveActorRef('foo')).toBe('foo');
  });

  test('it handles Links', () => {
    const link: Link = {
      type: "Link",
      href: "foo"
    };
    expect(Resolver.resolveActorRef(link)).toBe('foo')
  });

  test('it handles actors', () => {
    const actor: Partial<Actor> = {
      id: "foo",
      type: ObjectType.Person
    };
    expect(Resolver.resolveActorRef(actor)).toBe("foo");
  });
});

describe.skip('resolveActorRefs', () => {
  test('it handles arrays', () => {
    const value = ['a', 'b', 'c'];
    expect(Resolver.resolveActorRefs(value)).toEqual(value);
  });
  test('it handles single values', () => {
    const value = 'a';
    expect(Resolver.resolveActorRefs(value)).toEqual(value);
  });
});

describe.skip('resolveCollectionPageRef', () => {
  test('it handles string refs', () => {
    expect(Resolver.resolveCollectionPageRef("foo")).toBe("foo");
  });

  each([["Mention"], ["Link"]])
    .test('it handles %s', (type) => {
      const pageRef: CollectionPageRef<any> = {
        type,
        href: "foo"
      };
      expect(Resolver.resolveCollectionPageRef(pageRef)).toBe("foo");
    });

  each([["Collection"], ["CollectionPage"], ["OrderedCollection"], ["OrderedCollectionPage"]])
    .test('it handles %s', (type) => {
      const pageRef: CollectionPageRef<any> = {
        id: "foo",
        type
      };
      expect(Resolver.resolveCollectionPageRef(pageRef)).toBe("foo");
    })
});
