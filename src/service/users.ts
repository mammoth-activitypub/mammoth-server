'use strict';
import type {User} from '../struct/User';
import type {Actor} from 'mammoth-activitypub';

const Bcrypt = require('bcrypt');
const Content = require('../db/content.js');
const Couchdb = require('../db/couchdb');
const { generateKeyPairSync } = require('crypto');
const { ObjectType } = require('mammoth-activitystreams');

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const saveUserImpl = (user: User): Promise<any> => {
  const db = Couchdb.nano.use('users');
  return db.insert(user, user.userId);
};

const findUserImpl = (userId: string): Promise<User> => {
  const db = Couchdb.nano.use('users');
  return db.get(userId);
};

const saltRounds = 10;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
exports.createUser = async (user: User): Promise<any> => {
  user.hashedPassword = Bcrypt.hashSync(user.password, saltRounds);
  delete user.password;

  const {publicKey, privateKey} = generateKeyPairSync('rsa', {
    modulusLength: 2048,
    publicKeyEncoding: {
      type: 'spki',
      format: 'pem'
    },
    privateKeyEncoding: {
      type: 'pkcs8',
      format: 'pem',
    }
  });

  await Content.createContentDatabases(user);

  const id = Content.getActorDocumentIdFromActorId(user.userId);
  const actor: Actor = {
    id,
    type: ObjectType.Person,
    outbox: id + "/outbox",
    inbox: id + "/inbox",
    followers: id + "/followers",
    following: id + "/following",
    likes: id + "/likes",
    liked: id + "/liked",
    preferredUsername: user.userId,
    name: user.userId,
    publicKey: {
      id: id + '#main-key',
      owner: id,
      publicKeyPem: publicKey
    }
  };

  user.publicKey = publicKey;
  user.privateKey = privateKey;
  await Content.save(user, id, actor);
  return saveUserImpl(user);
};

exports.findUser = findUserImpl;

exports.saveUser = saveUserImpl;

exports.authenticate = async (userId: string, password: string): Promise<User> => {
  let user = null;
  try {
    user = await findUserImpl(userId);
  } catch (e) {
    return null;
  }

  const match = await Bcrypt.compareSync(password, user.hashedPassword);

  return match ? user : null;
};

exports.getActorDocumentIdFromActorId = (userId: string): string =>{
  return Content.getActorDocumentIdFromActorId(userId);
}

