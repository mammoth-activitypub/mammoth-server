import {
  Activity,
  ASObject,
  Collection,
  CollectionPage,
  ObjectRefs,
  OrderedCollection
} from 'mammoth-activitystreams';
const {ActorSubTypes, ObjectType, resolveLinkRef, resolveObjectRefs, resolveCollectionPageRef } = require('mammoth-activitystreams');
import {Actor, APObject} from 'mammoth-activitypub';
import {User} from '../struct/User';
import {Job, Processor} from 'bullmq';

export {};

let Content = require('../db/content');
const Config = require('config');
const Users = require('./users');
const Queues = require('./queues');
const crypto = require('crypto');
const Axios = require('axios');
const HttpSignature = require('http-signature');
const _ = require('lodash');

/**
 * Manages delivery of activities from a local Actor to other actors. It does this through a sequence of messages on
 * three queues:
 *
 * 1. The first queue are activities that need to be transmitted. These messages are interpreted to determine who are
 *    the relevant addressees; for example if you like a reply to a Note, the recipients should include the recipients
 *    of the original note, not just the author of the reply and their addressees.
 * 2. The second queue takes those addressees and expands them to actors' inboxes. If a an activity is sent to a user's
 *    Followers collection, this queue finds all the actors in that collection and queues all the actors in that
 *    collection, recursively, until they're all resolved to inboxes or a recursion maximum is reached.
 * 3. The third queue actually delivers the activity to an actor's inbox, after creating a digest and signing the
 *    message with http-signature.
 *
 *  This multi-layered queue structure helps insure that messages are delivered as quickly as possible to each
 *  inbox represented by an addressee. De-duplication of delivery is not currently supported, but could easily be added
 *  in a future release.
 */

/**
 * Take an addressee and queue them for expansion
 *
 * @param addressee The addressee, a URL linking to either an Actor or a Collection
 * @param userId The ID of the user whose data we're delivering
 * @param activity The activity to be delivered
 * @param recursionDepth The depth of recursion, which increases each time we expand into a collection
 */
async function enqueueAddressee(addressee: string, userId: string, activity: Activity, recursionDepth: number): Promise<Job> {
  if (addressee === 'https://www.w3.org/ns/activitystreams#Public') {
    return;
  }
  return Queues.addItem(
    'resolve-addressee',
    addressee,
    {
      userId,
      activity,
      addressee,
      recursionDepth
    });
}

/**
 * Queue an item for delivery to the inbox of an actor
 *
 * @param actor The actor to whose inbox we delivery the activity
 * @param user The user who sent the activity
 * @param activity The activity to deliver
 */
async function deliverToActor(actor: Actor, user: User, activity: Activity): Promise<void> {
  // no need to deliver to us messages from us
  if (actor.id !== Content.getActorDocumentIdFromActorId(user.userId)) {
    const destination = resolveLinkRef(actor.inbox);
    await Queues.addItem('deliver-activity', destination, {userId: user.userId, destination, activity});
  }
}

/**
 * Find all addressees in the collection and queue them for processing
 *
 * @param collection The collection we're addressing
 * @param userId The userId of the user who sent the message
 * @param activity The activity to deliver
 * @param recursionDepth The depth of recursion
 */
async function resolveCollectionAddressees(collection: OrderedCollection<Actor | Collection<ASObject>>, userId: string, activity: Activity, recursionDepth: number): Promise<void> {
  const items = [] as string[];
  if (collection.items) {
    items.push(...resolveObjectRefs(collection.items));
  }
  if (collection.orderedItems) {
    items.push(...resolveObjectRefs(collection.orderedItems));
  }
  await Promise.all(items.map(async (item) => {
    return enqueueAddressee(item, userId, activity, recursionDepth + 1);
  }));
}

/**
 * Queue the next page of the collection, if any
 *
 * @param collectionPage
 * @param userId
 * @param activity
 * @param recursionDepth
 */
async function resolveCollectionPagesAddressees(collectionPage: CollectionPage<Actor | Collection<ASObject>>, userId: string, activity: Activity, recursionDepth: number): Promise<void> {
  if (collectionPage.next) {
    await enqueueAddressee(resolveCollectionPageRef(collectionPage.next), userId, activity, recursionDepth);
  }
}

// convert an addressee (which could be an actor or a Collection of of actors e.g., the followers collection)
// detects and limits infinite recursion
// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function resolveAddressee(user: User, activity: Activity, addressee: string, recursionDepth = 0): Promise<any> {
  if (addressee === 'https://www.w3.org/ns/activitystreams#Public') {
    return;
  }
  const userId = user.userId;
  if (recursionDepth > 10) {
    return;
  }
  const object: APObject = await Content.fetchObject(addressee);

  if (ActorSubTypes.includes(object.type)) {
    await deliverToActor(object as Actor, user, activity);
  } else if (ObjectType.Collection === object.type || ObjectType.OrderedCollection === object.type) {
    await resolveCollectionAddressees(object as OrderedCollection<Actor | Collection<ASObject>>, userId, activity, recursionDepth);
  } else if (ObjectType.CollectionPage === object.type || ObjectType.OrderedCollectionPage === object.type) {
    await Promise.all([
      resolveCollectionAddressees(object as OrderedCollection<Actor | Collection<ASObject>>, userId, activity, recursionDepth),
      resolveCollectionPagesAddressees(object as CollectionPage<Actor | Collection<ASObject>>, userId, activity, recursionDepth)
    ]);
  }
}

function createTransformRequest(user: User, actor: Actor, destination: string) {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return function (data: any, headers: any): any {
    // reverse-engineered from the http-headers source code
    const req = {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      setHeader: (header: any, value: any): void => headers[header] = value,
      getHeader: (header: string): void =>
        headers[_.find(Object.keys(headers), (t: string) => t.toLowerCase() === header.toLowerCase())],
      method: 'POST',
      path: new URL(destination).pathname,
      hasOwnProperty: (): boolean => false
    };
    // noinspection TypeScriptValidateJSTypes
    HttpSignature.sign(req, {
      authorizationHeaderName: 'Signature',
      key: user.privateKey,
      keyId: actor.publicKey.id,
      headers: ['date', 'digest']
    });

    return data;
  };
}

function createDigest(json: string): string {
  return crypto.createHash('sha256').update(json).digest('base64');
}

/**
 * Generate a digest of each payload and include it in the header. Generate an http-signature of the date and the
 * digest to ensure that replay attacks are not possible. Deliver the message to the destination.
 *
 * @param user The user for whom we're delivering the message. Used for private key.
 * @param actor The actor for whom we're delivering the message.
 * @param activity The activity we're delivering.
 * @param destination The destination inbox where the message is to be delivered.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function deliverActivityTo(user: User, actor: Actor, activity: Activity, destination: string): Promise<any> {
  const activityJson = JSON.stringify(activity);
  const digest = createDigest(activityJson);
  const transformRequest = createTransformRequest(user, actor, destination);
  try {
    await Axios.post(destination, activityJson, {
      headers: {
        'Date': new Date().toUTCString(),
        'Digest': 'SHA-256=' + digest,
        'Content-Type': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
      },
      transformRequest
    });
    console.info(`activity ${activity.id} delivered to ${destination}`);
  } catch (e) {
    console.warn(`unable to deliver activity ${activity.id} to ${destination}`, e);
    throw e;
  }
}

function getAddressees(objectRefs: ObjectRefs<any>): string[] {
  if (!objectRefs) {
    return [];
  }
  const addressees = resolveObjectRefs(objectRefs);
  if (typeof addressees === 'string') {
    return [addressees];
  }
  return addressees;
}

/**
 * Find all the addressees in the various message fields and put them on the addressee queue, without blind
 * recipients.
 *
 * @param user
 * @param activity
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function queueAddressees(user: User, activity: Activity): Promise<any> {
  const addressees: string[] = [];
  addressees.push(...getAddressees(activity.to));
  addressees.push(...getAddressees(activity.bto));
  addressees.push(...getAddressees(activity.cc));
  addressees.push(...getAddressees(activity.bcc));
  addressees.push(...getAddressees(activity.audience));
  if (activity.bto) {
    delete activity.bto;
  }
  if (activity.bcc) {
    delete activity.bcc;
  }
  return Promise.all(addressees.map(addressee => enqueueAddressee(addressee, user.userId, activity, 0)));
}

/**
 * Copy the addressees of the activity's object and get them queued.
 *
 * @param user
 * @param activity
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function queueObjectAddressees(user: User, activity: Activity): Promise<any> {
  try {
    // todo need to search object actor, replies collection, inReplyTo, tags for additional recipients
    return queueAddressees(user, activity);
  } catch (e) {
    console.error('error queueing object addressees', e);
  }
}

/**
 * Handles sending activities from the main application. Gets the fields from the job and Finds the correct addresses for the message.
 * @param job
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const sendActivityProcessor: Processor = async (job: Job): Promise<any> => {
  const activity: Activity = job.data.activity;
  const userId: string = job.data.userId;
  const user: User = await Users.findUser(userId);
  switch (activity.type) {

    // queue jobs to deliver to the recipients of the object; update activity addressees
    case ObjectType.Delete:
    case ObjectType.Dislike:
    case ObjectType.Join:
    case ObjectType.Leave:
    case ObjectType.Like:
    case ObjectType.Reject:
    case ObjectType.TentativeAccept:
    case ObjectType.TentativeReject:
    case ObjectType.Undo:
    case ObjectType.Update:
      await queueObjectAddressees(user, activity);
      break;

    // queue jobs to deliver to the given addressees
    case ObjectType.Accept:
    case ObjectType.Announce:
    case ObjectType.Block:
    case ObjectType.Follow:
    case ObjectType.Create:
    case ObjectType.Invite:
    case ObjectType.Offer:
    case ObjectType.Question:
      await queueAddressees(user, activity);
      break;
  }
};

/**
 * Handles jobs for resolving a single addressee e.g. https://example.com/user/bob or https://example.com/user/bob/followers
 * @param job
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const resolveAddresseeProcessor: Processor = async (job: Job): Promise<any> => {
  const addressee: string = job.data.addressee;
  const activity: Activity = job.data.activity;
  const userId: string = job.data.userId;
  const user = await Users.findUser(userId);
  const recursionDepth: number = job.data.recursionDepth;

  return resolveAddressee(user, activity, addressee, recursionDepth);
};

/**
 * Handles jobs for delivering messages to a specific inbox e.g., https://example.com/user/bob/inbox.
 * @param job
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const deliverActivityProcessor: Processor = async (job: Job): Promise<any> => {
  const userId = job.data.userId as string;
  const user = await Users.findUser(userId);
  const actor = await Content.getById(user, Users.getActorDocumentIdFromActorId(userId));
  const activity = job.data.activity as Activity;
  const destination: string = job.data.destination;
  return deliverActivityTo(user, actor, activity, destination);
};


exports.configureQueues = function (): void {
  if (!queuesConfigured) {
    Queues.registerQueue('send-activity', sendActivityProcessor, 3, Config.app.transport.sendActivityJobsOptions);
    Queues.registerQueue('resolve-addressee', resolveAddresseeProcessor, 3, Config.app.transport.resolveAddresseeJobsOptions);
    Queues.registerQueue('deliver-activity', deliverActivityProcessor, 10, Config.app.transport.deliverActivityJobsOptions);
    queuesConfigured = true;
  }
}

let queuesConfigured = false;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
exports.sendActivity = function (userId: string, activity: Activity): Promise<Job<any, any>> {
  exports.configureQueues();

  return Queues.addItem('send-activity', `deliver ${activity.type} activity ${activity.id}`, {activity, userId});
}
