'use strict';
/// <reference types="jest" />
/// <reference types="rewire" />

import {User} from '../struct/User';
import {ASObject, Delete, Follow, ObjectType} from 'mammoth-activitystreams';

const rewire = require('rewire');
const Joi = require('@hapi/joi');

const ActivityDispatcher = rewire('../../dist/service/activity-dispatcher');

test('user-submitted delete requests are configured', () => {

  const configureUserSubmittedDelete = ActivityDispatcher.__get__('configureUserSubmittedDelete');

  const _delete: Partial<Delete> = {
    id: 'id',
    type: ObjectType.Delete,
  };
  const user: Partial<User> = {
    userId: 'foo'
  };
  configureUserSubmittedDelete(user, _delete);
  expect(_delete.published).not.toBeUndefined();
  expect(_delete.updated).not.toBeUndefined();
  expect(_delete.deleted).not.toBeUndefined();
});

test('Unknown activity types yield an error', async () => {
  const user: Partial<User> = { };
  const object = {
    type: "Slartibartfast"
  } as unknown as ASObject;
  await expect(ActivityDispatcher.handlePostedDocument(user, false, object)).rejects.toBeDefined();
});

describe('follows', () => {
  describe('joi validations for follows', () => {
    function goodMessage(): Follow {
      return {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "http://my.message/id",
        "summary": "Sally followed John",
        "type": ObjectType.Follow,
        "actor": "http://me.here/",
        "object": "http://you.there",
      }
    }

    test('a simple follow works', () => {
      const followActivityValidation = ActivityDispatcher.__get__('followActivityValidation');
      expect(() => Joi.assert(goodMessage(), followActivityValidation)).not.toThrow();
    });

    test('missing actor should be an error', () => {
      const followActivityValidation = ActivityDispatcher.__get__('followActivityValidation');
      const follow = goodMessage();
      delete follow.actor;
      expect(() => Joi.assert(follow, followActivityValidation)).toThrow();
    });

    test('missing object should be an error', () => {
      const followActivityValidation = ActivityDispatcher.__get__('followActivityValidation');
      const follow = goodMessage();
      delete follow.object;
      expect(() => Joi.assert(follow, followActivityValidation)).toThrow();
    });

    test('missing id should be an error', () => {
      const followActivityValidation = ActivityDispatcher.__get__('followActivityValidation');
      const follow = goodMessage();
      delete follow.id;
      expect(() => Joi.assert(follow, followActivityValidation)).toThrow();
    });

    test('missing @context should not be an error', () => {
      const followActivityValidation = ActivityDispatcher.__get__('followActivityValidation');
      const follow = goodMessage();
      delete follow['@context'];
      expect(() => Joi.assert(follow, followActivityValidation)).not.toThrow();
    });

  });
});
