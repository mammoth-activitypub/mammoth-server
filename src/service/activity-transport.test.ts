'use strict';

import {Activity, ObjectRefs} from 'mammoth-activitystreams';
import {User} from '../struct/User';

const rewire = require('rewire');

let ActivityTransport = rewire('../../dist/service/activity-transport');

beforeEach(() => {
  ActivityTransport = rewire('../../dist/service/activity-transport');
});

describe('resolveCollectionAddresses', () => {
  test('all items and orderedItems are gathered and enqueued', async () => {
    const collection = {
      items: [
        'a', 'b'
      ],
      orderedItems: [
        'c', 'd'
      ]
    };
    const activity = {
      id: "foo"
    };
    const userId = "dude";
    const enqueueAddressee = jest.fn();
    ActivityTransport.__set__('enqueueAddressee', enqueueAddressee);
    const resolveCollectionAddressees = ActivityTransport.__get__('resolveCollectionAddressees');

    await resolveCollectionAddressees(collection, userId, activity, 0);

    expect(enqueueAddressee).toHaveBeenCalledWith('a', userId, activity, 1);
    expect(enqueueAddressee).toHaveBeenCalledWith('b', userId, activity, 1);
    expect(enqueueAddressee).toHaveBeenCalledWith('c', userId, activity, 1);
    expect(enqueueAddressee).toHaveBeenCalledWith('d', userId, activity, 1);
  });
});

describe('getAddressees', () => {
  let getAddressees: (o: ObjectRefs<any>) => string[];
  beforeEach(() => {
    getAddressees = ActivityTransport.__get__('getAddressees');
  });
  it('returns an array for a single addressee', () => {
    expect(getAddressees("foo")).toEqual(["foo"]);
  });
  it('returns an array of resolved addressess when given an array', () => {
    // @ts-ignore
    expect(getAddressees(['foo', 'bar'])).toEqual(['foo', 'bar']);
  });
});

describe('resolveAddressee', () => {
  const user = {
    userId: 'bob'
  } as User;

  const activity = {
    id: 'activityId',
    type: 'Create'
  } as Activity;

  const addressee = 'http://nowhere/bob';

  let resolveAddressee: any;

  const fetchObject = jest.fn();
  const deliverToActor = jest.fn();
  const resolveCollectionAddressees = jest.fn();
  const resolveCollectionPagesAddressees = jest.fn();

  beforeEach(() => {
    ActivityTransport.__set__('Content', {fetchObject});
    ActivityTransport.__set__('deliverToActor', deliverToActor);
    ActivityTransport.__set__('resolveCollectionAddressees', resolveCollectionAddressees);
    ActivityTransport.__set__('resolveCollectionPagesAddressees', resolveCollectionPagesAddressees);
    resolveAddressee = ActivityTransport.__get__('resolveAddressee');
    fetchObject.mockReset();
    deliverToActor.mockReset();
    resolveCollectionAddressees.mockReset();
    resolveCollectionPagesAddressees.mockReset();
  });

  it('gives up when maximum recursion level is reached', async () => {
    await expect(resolveAddressee(user, activity, addressee, 11));
  });

  it('handles actors', async () => {
    const actor = {
      id: addressee,
      type: "Person"
    };

    fetchObject.mockResolvedValue(actor);

    await resolveAddressee(user, activity, addressee, 0);
    expect(fetchObject).toHaveBeenCalledWith(addressee);
    expect(deliverToActor).toHaveBeenCalledWith(actor, user, activity);
  });

  test('handles Collections', async () => {
    const collection = {
      id: addressee,
      type: "Collection"
    };

    fetchObject.mockResolvedValue(collection);

    await expect(resolveAddressee(user, activity, addressee, 0)).resolves.not.toThrow();
    expect(fetchObject).toHaveBeenCalledWith(addressee);
    expect(resolveCollectionAddressees).toHaveBeenCalledWith(collection, user.userId, activity, 0);
  });

  test('handles PagedCollections', async () => {
    const collection = {
      id: addressee,
      type: "CollectionPage",
      next: "http://nowhere/bob/next"
    };

    fetchObject.mockResolvedValue(collection);

    await expect(resolveAddressee(user, activity, addressee, 0)).resolves.not.toThrow();

    expect(fetchObject).toHaveBeenCalledWith(addressee);
    expect(resolveCollectionAddressees).toHaveBeenCalledWith(collection, user.userId, activity, 0);
    expect(resolveCollectionPagesAddressees).toHaveBeenCalledWith(collection, user.userId, activity, 0);
  });
});
