"use strict";
/// <reference types="hapi" />

export {};

const consola = require("consola");
const Hapi = require("@hapi/hapi");
const Boom = require("@hapi/boom");
const Joi = require("@hapi/joi");
const Config = require("config");
const Content = require("./db/content");
const Couchdb = require("./db/couchdb");
const InboxRoutes = require("./route/inbox-routes");
const ImageUploads = require("./route/image-uploads");
const Users = require('./service/users');
const UserRoutes = require("./route/user-routes");
const OutboxRoutes = require("./route/outbox-routes");
const ActorRoutes = require("./route/actor-routes");
const AdminRoutes = require("./route/admin-routes");
const moment = require("moment");
const ActivityTransport = require('./service/activity-transport');
const axios = require('axios');

Date.prototype.toJSON = function (): string {
  return moment(this).format();
};

async function start(): Promise<void> {
  axios.defaults.headers.common['User-Agent'] = 'mammoth/0.0.1';

  const server = Hapi.server({
    host: Config.app.host,
    port: Config.app.port,
    routes: {
      cors: true,
      validate: {
        failAction: (request: any, h: any, err: any): void => {
          if (process.env.NODE_ENV === "production") {
            // In prod, log a limited error message and throw the default Bad Request error.
            throw Boom.badRequest("Invalid request payload input");
          }

          // During development, log and respond with the full error.
          consola.error(err);
          throw err;
        },
      },
    },
  });

  await server.register(require("@hapi/cookie"));

  server.auth.strategy("session", "cookie", {
    keepAlive: true,
    cookie: {
      password: Config.app.auth.secret, // cookie secret
      path: "/",
      isSecure: process.env.NODE_ENV === "production",
      name: "mammoth-auth", // Cookie name
      ttl: 24 * 60 * 60 * 1000 // Set session to 1 day
    },
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    validateFunc: async (request: any, session: any) => {
      const user = await Users.findUser(session.userId);
      if (!user) {
        return { valid: false };
      }

      return { valid: true, credentials: user };
    },
  });

  server.auth.default("session");
  server.validator(Joi);

  ActivityTransport.configureQueues();
  InboxRoutes.init(server);
  UserRoutes.init(server);
  OutboxRoutes.init(server);
  ActorRoutes.init(server);
  ImageUploads.init(server);
  AdminRoutes.init(server);

  const requestableCollections = ["likes", "dislikes", "shares"];
  server.route({
    method: "GET",
    path: "/a/{actor}/{id}/{collection}",
    options: {
      auth: false,
    },
    async handler(request: any) {
      const actorId = request.params.actor;
      const user = await Users.findUser(actorId); // fixme might not be an actor
      const objectId = request.params.id;
      const collection = request.params.collection;
      if (!requestableCollections.includes(collection)) {
        return Boom.badRequest("bad collection", collection);
      }
      const key = `${Content.getActorDocumentIdFromActorId(actorId)}/${objectId}`;
      try {
        return await Content.getCollectionForObject(user, key, collection);
      } catch (e) {
        throw Boom.boomify(e);
      }
    },
  });

  await server.start();

  consola.ready({
    message: `Server running at: ${server.info.uri}`,
    badge: true,
  });
}

process.on("unhandledRejection", (error) => consola.error(error));

Couchdb.setup();
start();
