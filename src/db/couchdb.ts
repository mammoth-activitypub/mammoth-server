'use strict';

import {User} from '../struct/User';
import * as nano from 'nano';
import {ASObject} from 'mammoth-activitystreams';

export {};

const Config = require('config');
const consola = require('consola');
let Nano: nano.ServerScope = Config.db ? require('nano')(Config.db.url): undefined;
const Crypto = require('crypto');

module.exports = {
  nano: Nano,
  setup: async (): Promise<void> => {
    await configDb();
  },
  updateDbSchema,
  save
};

async function configDb(): Promise<void> {
  try {
    await Nano.db.get('users');
  } catch (e) {
    consola.log('creating user db...');
    await Nano.db.create('users');
    consola.log('user db created');
  }
}

const METADATA_KEY = 'db_metadata';
const CURRENT_METADATA_VERSION = 0;

type Metadata = {
  _id: 'db_metadata';
  _rev?: string;
  type: 'db_metadata';
  version: number;
  viewVersion: {
    [index: string]: string;
  };
  updates: string[];
}

const databasesSeen: Record<string, Date> = {};

function getUsersDatabase(user: User): nano.DocumentScope<any> {
  return Nano.db.use(user.userDb);
}

async function getById(user: User, key: string): Promise<ASObject> {
  try {
    const db = await getUsersDatabase(user);
    return await db.get(key);
  } catch (e) {
    if (e.statusCode === 404) {
      return null;
    }
    throw e;
  }
}

async function save(user: User, key: string, document: any, rev?: string): Promise<nano.DocumentInsertResponse> {
  document._id = key;
  if (rev) {
    document._rev = rev;
  }
  const db = await getUsersDatabase(user);
  return db.insert(document);
}

function hashSource(source: string): string {
  return Crypto.createHash('sha1').update(source).digest('hex');
}

function updateSourcecodeIfNeeded(
  idealDesignDocViews: Record<string, Record<string, (doc: any) => void>>,
  actualDesignDocViews: Record<string, Record<string, string>>,
  viewName: string,
  functionName: string,
  sourceCodeUpdater?: (source: string) => string): boolean {
  const decompiledFunctionSource = idealDesignDocViews[viewName][functionName].toString();
  const idealSourceCode = sourceCodeUpdater ? sourceCodeUpdater(decompiledFunctionSource) : decompiledFunctionSource;
  const idealSourceCodeHash = hashSource(idealSourceCode);
  let actualSourceCodeHash = '';
  if (!actualDesignDocViews[viewName]) {
    actualDesignDocViews[viewName] = {};
  }
  if (actualDesignDocViews[viewName][functionName]) {
    const actualSourceCode = actualDesignDocViews[viewName][functionName];
    actualSourceCodeHash = hashSource(actualSourceCode);
  }
  if (idealSourceCodeHash !== actualSourceCodeHash) {
    // update schema and metadata record
    actualDesignDocViews[viewName][functionName] = idealSourceCode;
    return true;
  }
  return false;
}

export interface DatabaseUpdate {
  id: string;
  name: string;
  code: (user: User, db: nano.DocumentScope<any>) => Promise<void>;
}

async function updateDbSchema(user: User, idealDesignDoc: any, sourceCodeUpdater?: (source: string) => string, updates?: DatabaseUpdate[]): Promise<void> {
  const userId = user.userId;
  const key = userId + '/' + idealDesignDoc._id;
  if (!databasesSeen[key]) {
    databasesSeen[key] = new Date();
    let metadata: Metadata = await getById(user, 'db_metadata') as unknown as Metadata;

    if (!metadata) {
      metadata = {
        _id: METADATA_KEY,
        type: METADATA_KEY,
        version: CURRENT_METADATA_VERSION,
        viewVersion: {},
        updates: []
      };
    }
    let actualDesignDoc = await getById(user, idealDesignDoc._id);

    if (!actualDesignDoc) {
      actualDesignDoc = {
        _id: idealDesignDoc._id,
        views: {},
        language: 'javascript'
      } as any;
    }
    let changes = false;
    for (let viewName in idealDesignDoc.views) {
      if (idealDesignDoc.views.hasOwnProperty(viewName)) {
        changes = updateSourcecodeIfNeeded(
          idealDesignDoc.views,
          actualDesignDoc.views,
          viewName,
          'map',
          sourceCodeUpdater) || changes;
        // @ts-ignore
        if (idealDesignDoc.views[viewName].reduce) {
          changes = updateSourcecodeIfNeeded(
            idealDesignDoc.views,
            actualDesignDoc.views,
            viewName,
            'reduce',
            sourceCodeUpdater) || changes;
        }
      }
    }
    const db = getUsersDatabase(user);
    if (updates) {
      for (let i = 0; i < updates.length; i++) {
        if (!metadata.updates) {
          metadata.updates = [];
        }
        if (i < metadata.updates.length) {
          if (updates[i].id !== metadata.updates[i]) {
            throw Error(`updates out of sequence; expected ${updates[i].id} but found ${metadata.updates[i]}`);
          }
        }
        await updates[i].code(user, db);
        metadata.updates.push(updates[i].id);
        metadata._rev = (await db.insert(metadata)).rev;
      }
    }
    if (changes) {
      try {
        await db.insert(actualDesignDoc);
        await db.insert(metadata);
      } catch (e) {
        console.error('error updating database with new schema', e);
      }
    }
  }
}

