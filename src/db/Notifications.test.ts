import {Activity, ASObject, Like, Note, ObjectRef, ObjectType} from 'mammoth-activitystreams';
import {User} from '../struct/User';
import each from 'jest-each';

const rewire = require('rewire');

const Notifications = rewire('../../dist/db/Notifications');
const Content = {
  resolveObject: jest.fn(),
  getActorDocumentIdFromActorId: jest.fn()
};

describe('notifications', () => {
  beforeEach(() => {
    Notifications.__set__('Content', Content);
    Content.resolveObject.mockReset();
    Content.getActorDocumentIdFromActorId.mockReset();
  });

  const noteId = 'https://example.com/id';
  const noteActor = 'https://example.com/note-actor';
  const note: Partial<Note> = {
    id: noteId,
    type: ObjectType.Note,
    attributedTo: noteActor,
    content: 'The quick brown fox'
  };
  const like: Partial<Like> = {
    id: 'https://example.com/like',
    type: ObjectType.Like,
    object: noteId,
    actor: 'https://example.com/like-actor'
  };
  const user: Partial<User> = {
    userId: 'noteActor'
  };

  function createActivity(type: string): Activity {
    return {
      id: 'https://example.com/' + type,
      type: type as any,
      object: noteId,
      actor: `https://example.com/${type}-actor`
    } as Activity;
  }

  describe('actedOnAnObjectUserCreated', () => {
    const actedOnAnObjectUserCreated: (user: User, objectRef: ObjectRef<ASObject>) => Promise<string>
      = Notifications.__get__('actedOnAnObjectUserCreated');
    const activity = createActivity('Create');

    test('an object of an item that is created by the user returns true', () => {
      Content.resolveObject.mockResolvedValue(note);
      Content.getActorDocumentIdFromActorId.mockReturnValue(noteActor);
      return expect(actedOnAnObjectUserCreated(user as User, activity)).resolves.toBe('RepliedToUsersPost');
    });

    test('an object of an item that is not created by the user returns null', () => {
      Content.resolveObject.mockResolvedValue(note);
      Content.getActorDocumentIdFromActorId.mockReturnValue(noteActor + 'butnotreally');
      expect(actedOnAnObjectUserCreated(user as User, activity)).resolves.toBeNull();
    });

    test('an object of an item that we cannot find returns null', () => {
      Content.resolveObject.mockResolvedValue(null);
      Content.getActorDocumentIdFromActorId.mockReturnValue(noteActor);
      expect(actedOnAnObjectUserCreated(user as User, activity)).resolves.toBeNull();
    });
  });

  describe('handleCreateInReplyToSomethingTheUserDid', () => {
    const handleCreateInReplyToSomethingTheUserDid: (object: ASObject) => string = Notifications.__get__('handleCreateInReplyToSomethingTheUserDid');
    it('returns correct notification when reply is to a post', () => {
      const object = {
        type: ObjectType.Create
      };
      expect(handleCreateInReplyToSomethingTheUserDid(object)).toBe('RepliedToUsersPost');
    });
    it('returns correct notification when reply is to a reply', () => {
      const object = {
        type: ObjectType.Create,
        inReplyTo: 'https://example.com/foo'
      };
      expect(handleCreateInReplyToSomethingTheUserDid(object)).toBe('RepliedToUsersReply');
    });
  });

  describe('should create notification', () => {
    const userNotifiedAlready = jest.fn();
    const actedOnAnObjectUserCreated = jest.fn();

    beforeEach(() => {
      Notifications.__set__('userNotifiedAlready', userNotifiedAlready);
      Notifications.__set__('actedOnAnObjectUserCreated', actedOnAnObjectUserCreated);
    });

    describe('activities of an object attributed to an actor should notify the actor', () => {
      each([[ObjectType.Like], [ObjectType.Announce], [ObjectType.Accept], [ObjectType.TentativeAccept],
        [ObjectType.Reject], [ObjectType.TentativeReject]])
        .it('when the activity is \'%s\'', async (objectType: ObjectType) => {

          userNotifiedAlready.mockResolvedValue(false);

          const user: Partial<User> = {
            userId: 'noteActor'
          };

          actedOnAnObjectUserCreated.mockResolvedValue(true);

          const activity = createActivity(objectType);
          expect(await Notifications.shouldCreateNotification(user, activity)).toBeTruthy();
          expect(actedOnAnObjectUserCreated).toHaveBeenCalledWith(user, activity);
        });
    });
  });

  describe('handleLikeOfSomethingUserDid', () => {
    let handleLikeOfSomethingUserDid: (object: ASObject) => string;
    beforeEach(() => {
      handleLikeOfSomethingUserDid = Notifications.__get__('handleLikeOfSomethingUserDid');
    });

    it('detects a post was liked', () => {
      const object = {
        type: ObjectType.Note
      };
      expect(handleLikeOfSomethingUserDid(object as ASObject)).toBe('LikedUsersPost');
    });

    it('detects a reply was liked', () => {
      const object = {
        type: ObjectType.Note,
        inReplyTo: 'https://foo/bar'
      };
      expect(handleLikeOfSomethingUserDid(object as ASObject)).toBe('LikedUsersReply');
    });
  });

  describe('handleCreateInReplyToSomethingTheUserDid', () => {
    let handleCreateInReplyToSomethingTheUserDid: (object: ASObject) => string;
    beforeEach(() => {
      handleCreateInReplyToSomethingTheUserDid = Notifications.__get__('handleCreateInReplyToSomethingTheUserDid');
    });

    it('detects a post was replied to', () => {
      const object = {
        type: ObjectType.Note
      };
      expect(handleCreateInReplyToSomethingTheUserDid(object as ASObject)).toBe('RepliedToUsersPost');
    });

    it('detects a reply was replied to', () => {
      const object = {
        type: ObjectType.Note,
        inReplyTo: 'https://foo/bar'
      };
      expect(handleCreateInReplyToSomethingTheUserDid(object as ASObject)).toBe('RepliedToUsersReply');
    });
  });

  describe('handleAcceptOfSomethingUserDid', () => {
    let handleAcceptOfSomethingUserDid: (object: ASObject) => string;
    beforeEach(() => {
      handleAcceptOfSomethingUserDid = Notifications.__get__('handleAcceptOfSomethingUserDid');
    });

    it('detects a follow was accepted', async () => {
      const object = {
        type: ObjectType.Follow
      };
      expect(await handleAcceptOfSomethingUserDid(object as ASObject)).toBe('AcceptedUsersFollowRequest');
    });

    it('detects a reply was replied to', async () => {
      const object = {
        type: ObjectType.Invite,
        target: 'https://some/target'
      };
      const target = {
        type: ObjectType.Relationship
      }
      Content.resolveObject.mockResolvedValue(target);
      expect(await handleAcceptOfSomethingUserDid(object as ASObject)).toBe('AcceptedUsersFriendRequest');
      expect(Content.resolveObject).toHaveBeenCalledWith(object.target);
    });
  });

  describe('actedOnTheUserActor', () => {
    const user: User = {
      userId: 'bob'
    } as User;
    let actedOnTheUserActor: (user: User, activity: Activity) => Promise<string>;
    beforeEach(() => {
      actedOnTheUserActor = Notifications.__get__('actedOnTheUserActor');
    });

    it('doesn’t notify of actions upon actors other than the user', () => {
      Content.getActorDocumentIdFromActorId.mockReturnValue('https://someone');
      const activity: Activity = {
        type: ObjectType.Follow,
        object: {
          type: ObjectType.Person,
          id: 'https://someone/else'
        }
      };
      expect(actedOnTheUserActor(user, activity)).resolves.toBeNull();
    });

    it ('returns the right notification type when another user sends a follow request', () => {
      Content.getActorDocumentIdFromActorId.mockReturnValue('https://someone');
      const activity: Activity = {
        type: ObjectType.Follow,
        object: {
          type: ObjectType.Person,
          id: 'https://someone'
        }
      };
      expect(actedOnTheUserActor(user, activity)).resolves.toBe('SentUserAFollowRequest');
    });

    it ('returns the right notification type when another user sends a friend request', () => {
      Content.getActorDocumentIdFromActorId.mockReturnValue('https://someone');
      const target: ASObject = {
        id: 'http://some/relationship',
        type: ObjectType.Relationship
      };
      const activity: Activity = {
        type: ObjectType.Invite,
        object: 'https://someone',
        target: target.id
      };
      Content.resolveObject.mockResolvedValue(target);
      expect(actedOnTheUserActor(user, activity)).resolves.toBe('SentUserAFriendRequest');
      expect(Content.resolveObject).toHaveBeenCalledWith(activity.target, null, user);
    });
  });
});
