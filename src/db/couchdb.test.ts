import {User} from '../struct/User';
import * as nano from 'nano';
import {ASObject} from 'mammoth-activitystreams';

const rewire = require('rewire');

let couchdb: any;
const Nano = {
  db: {
    get: jest.fn(),
    create: jest.fn(),
    use: jest.fn()
  }
};

const getUsersDatabase = jest.fn();

beforeEach(() => {
  Nano.db.get.mockReset();
  Nano.db.create.mockReset();
  Nano.db.use.mockReset();
  getUsersDatabase.mockReset();
  couchdb = rewire('../../dist/db/couchdb');
  couchdb.__set__('Nano', Nano);
});

describe('configDb', () => {
  let configDb: () => Promise<void>;
  beforeEach(() => {
    configDb = couchdb.__get__('configDb');
  });
  it('creates user db if not present', async () => {
    Nano.db.get.mockRejectedValue('no!');
    await configDb();
    expect(Nano.db.get).toHaveBeenCalled();
    expect(Nano.db.create).toHaveBeenCalledWith('users');
  });

  it ('doesn’t create user db if present', async() => {
    Nano.db.get.mockResolvedValue('db');
    await configDb();
    expect(Nano.db.get).toHaveBeenCalled();
    expect(Nano.db.create).not.toHaveBeenCalled();
  });
});

it('returns the user database when asked', () => {
  const getUsersDatabase: (user: User) => nano.DocumentScope<any> = couchdb.__get__('getUsersDatabase');
  const user: Partial<User> = {
    userDb: 'mydb'
  }
  const db = {
    foo: 'bar'
  };
  Nano.db.use.mockReturnValue(db);
  expect(getUsersDatabase(user as User)).toBe(db);
  expect(Nano.db.use).toHaveBeenCalledWith(user.userDb);
});

describe('getById', () => {
  let getById: (user: User, key: string) => Promise<ASObject>;
  const db = {
    get: jest.fn()
  }
  const user = {
    userId: 'bob'
  }
  const key = 'https://example.com/bob/12345';
  const expectedResult = {
    foo: 'bar'
  }

  beforeEach(() => {
    couchdb.__set__('getUsersDatabase', getUsersDatabase);
    getById = couchdb.__get__('getById');
  });

  it('returns the value from the db when found', async () => {
    getUsersDatabase.mockReturnValue(db);
    db.get.mockResolvedValue(expectedResult);
    expect(await getById(user as User, key)).toBe(expectedResult);
    expect(getUsersDatabase).toHaveBeenCalledWith(user);
  });

  it('returns null when not found', async () => {
    getUsersDatabase.mockReturnValue(db);
    const error = {
      statusCode: 404
    };
    db.get.mockRejectedValue(error);
    expect(await getById(user as User, key)).toBeNull();
    expect(getUsersDatabase).toHaveBeenCalledWith(user);
  });
});

it('hashes text with sha1', () => {
  const hashSource: (s: string) => string = couchdb.__get__('hashSource');
  expect(hashSource('foo')).toBe('0beec7b5ea3f0fdbc95d0dd47f3c5bc275da8a33');
});

