/* eslint-disable */
'use strict';
/// <reference types="nano" />

import {FetchInstructions} from 'mammoth-api';

const {CollectionSubTypes,
  objectRefsType,
  objectRefType,
  ObjectType,
  resolveActorRef,
  resolveLinkRef,
  resolveObjectRef} = require('mammoth-activitystreams');
const {FetchInstruction} = require('mammoth-api');

import {
  Activity,
  ActorRef,
  ActorRefs,
  ASObject,
  Collection,
  Link,
  LinkRef,
  ObjectRef,
  ObjectRefs,
  Relationship
} from 'mammoth-activitystreams';
import {User} from '../struct/User';
import {Actor, APObject} from 'mammoth-activitypub';
import * as Nano from 'nano';
import {DocumentViewParams} from 'nano';
import {DatabaseUpdate} from './couchdb';

export {};

const Couchdb = require('./couchdb');
const Config = require('config');
const userDesignDoc = 'content-views';
const timelineView = 'timeline';
const publicTimelineView = 'publicTimeline';
const contentToPostIdView = 'contentToPostId';
const activityIdToObjectIdView = 'activityIdToObjectId';
const objectCollectionsView = 'objectCollections';
let Axios = require('axios');
const Ioredis = require('ioredis');

let ioredis = Config.redis?.connection ? new Ioredis(Config.redis.connection) : null;
const HttpSignature = require('http-signature');
const _ = require('lodash');


// eslint-disable-next-line @typescript-eslint/no-unused-vars
function emit(...vars: any[]): any {
  return vars;
}

function userDesignDocDef(actorId: string): any {
  return {
    _id: '_design/' + userDesignDoc,
    views: {
      timeline: {
        map: function (doc: APObject): void {
          if (['Article', 'Audio', 'Event', 'Image', 'Note', 'Page', 'Video'].includes(doc.type) && !doc.inReplyTo) {
            emit(doc.published);
          } else if (doc.type === 'Announce' && doc.object.attributedTo !== actorId) {
            emit(doc.published);
          }
        }
      },
      publicTimeline: {
        map: function (doc: APObject): void {
          const aid = actorId;

          function isPubliclyAddressed(addressees: any): boolean {
            if (!addressees) {
              return false;
            }
            if (typeof addressees === 'string') {
              return (addressees as string) === 'https://www.w3.org/ns/activitystreams#Public';
            }
            return (addressees as string[]).includes('https://www.w3.org/ns/activitystreams#Public');
          }

          function isPublic(post: APObject): boolean {
            return isPubliclyAddressed(post.to) ||
              isPubliclyAddressed(post.cc);
          }

          function isMyPost(post: APObject): boolean {
            return post.attributedTo === aid ||
              post.actor === aid;
          }

          const ActivitySubTypes = [
            'Accept',
            'TentativeAccept',
            'Add',
            'Arrive',
            'Create',
            'Delete',
            'Follow',
            'Ignore',
            'Join',
            'Leave',
            'Offer',
            'Invite',
            'Reject',
            'TentativeReject',
            'Remove',
            'Undo',
            'Update',
            'View',
            'Listen',
            'Read',
            'Move',
            'Travel',
            'Announce',
            'Block',
            'Flag',
            'Like',
            'Dislike',
            'Question'
          ];

          function isActivity(post: APObject): boolean {
            return ActivitySubTypes.includes(post.type);
          }

          if (isActivity(doc) && isMyPost(doc) && isPublic(doc)) {
            emit(doc.published);
          }
        }
      },
      // what are activities sent by us about others?
      activityOf: {
        map: function (doc: ASObject): void {
          if (doc.actor === actorId) {
            if (doc.object) {
              if (typeof doc.object === 'string') {
                emit(doc.object);
              } else if (doc.object.id) {
                emit(doc.object.id);
              }
            }
          }
        }
      },
      // inverse of the above: what are activities sent by others about us?
      activityFrom: {
        map: function (doc: ASObject): void {
          if (doc.object === actorId) {
            if (doc.actor) {
              if (typeof doc.actor === 'string') {
                emit(doc.actor);
              } else if (doc.actor.id) {
                emit(doc.actor.id);
              }
            }
          }
        }
      },
      relationship: {
        map: function (doc: ASObject): void {
          if (doc.type === 'Relationship') {
            if (doc.object === actorId) {
              emit(doc.subject);
            }
            if (doc.subject === actorId) {
              emit(doc.object);
            }
          }
        }
      },
      objectCollections: {
        map: function (doc: Activity): void {
          function linkRefType(linkRef: LinkRef): string {
            if (typeof linkRef === 'string') {
              return 'string';
            }
            return 'Link';
          }

          function objectRefType(objectRef: ObjectRef<ASObject>): string {
            if (typeof objectRef === 'string') {
              return 'string';
            }
            if (objectRef.type === 'Link') {
              return linkRefType(objectRef as LinkRef);
            }
            return 'ASObject';
          }

          function resolveLinkRef(linkRef: LinkRef): string {
            switch (linkRefType(linkRef)) {
              case 'string':
                return linkRef as string;
              case 'Link':
                return (linkRef as Link).href;
            }
            return null;
          }

          function resolveObjectRef(objectRef: ObjectRef<ASObject>): string {
            switch (objectRefType(objectRef)) {
              case 'string':
                return objectRef as string;
              case 'Link':
                return (objectRef as Link).href;
              case 'ASObject':
                return resolveLinkRef((objectRef as ASObject).id);
            }
            return null;
          }

          const ActorSubTypes = [
            'Actor',
            'Application',
            'Group',
            'Organization',
            'Person',
            'Service'
          ];

          function actorRefType(actorRef: ActorRef): string {
            if (actorRef && (actorRef as Actor).type && ActorSubTypes.includes((actorRef as Actor).type)) {
              return 'Actor';
            }
            return linkRefType(actorRef as LinkRef);
          }

          function actorRefsType(actorRefs: ActorRefs): string {
            if (Array.isArray(actorRefs)) {
              return 'array';
            }
            return actorRefType(actorRefs);
          }

          function resolveActorRef(actorRef: ActorRef): string {
            switch (actorRefType(actorRef)) {
              case 'string':
                return actorRef as string;
              case 'Link':
                return (actorRef as Link).href;
              case 'Actor':
                return resolveLinkRef((actorRef as Actor).id);
            }
            return null;
          }

          function resolveActorRefs(actorRefs: ActorRefs): string[] {
            if (!actorRefs) {
              return null;
            }
            if (actorRefsType(actorRefs) === 'array') {
              const actorTypes = [] as string[];
              const actorRefsArray = actorRefs as ActorRef[];
              for (let i = 0; i < actorRefsArray.length; i++) {
                actorTypes.push(resolveActorRef(actorRefsArray[i]));
              }
              return actorTypes;
            }
            return [resolveActorRef(actorRefs as ActorRef)];
          }

          if (doc.inReplyTo) {
            const ref = resolveObjectRef(doc.inReplyTo);
            if (ref) {
              emit([ref, 'replies', doc.published || '9999']);
            }
          }

          if (doc.object) {
            const objectRef = resolveObjectRef(doc.object);
            switch (doc.type) {
              case 'Like':
                emit([objectRef, 'likes', doc.published || '9999']);
                break;

              case 'Dislike':
                emit([objectRef, 'dislikes', doc.published || '9999']);
                break;

              case 'Announce':
                emit([objectRef, 'shares', doc.published || '9999']);
                break;

              case 'Accept':
                if ((doc.object as Activity).type === 'Follow') {
                  const actorsWhoAccepted = resolveActorRefs(doc.actor);
                  const actorsWhoFollowed = resolveActorRefs((doc.object as ASObject).actor);
                  for (let i = 0; i < actorsWhoAccepted.length; i++) {
                    const actorWhoAccepted = actorsWhoAccepted[i];
                    for (let j = 0; j < actorsWhoFollowed.length; j++) {
                      const actorWhoFollowed = actorsWhoFollowed[j];
                      emit([actorWhoAccepted, 'following', doc.published || '9999']);
                      emit([actorWhoFollowed, 'followers', doc.published || '9999']);
                    }
                  }
                }
                break;
            }
          }
        }
      },
      activityIdToObjectId: {
        map: function (doc: Activity): void {
          if (doc.object) {
            if (typeof doc.object === 'string') {
              emit(doc.object);
            } else if (doc.object.type === 'Link') {
              emit((doc.object as Link).href);
            } else {
              emit((doc.object as APObject).id);
            }
          }
        }
      },
      tagToObject: {
        map: function (doc: ASObject): void {
          if (doc.tag) {
            if (Array.isArray(doc.tag)) {
              (doc.tag as any[])
                .forEach((tag: any) => {
                  if (tag.type === 'Hashtag') {
                    let hashTag: string = tag.name.trim().toLowerCase();
                    if (hashTag.startsWith('#')) {
                      hashTag = hashTag.slice(1);
                    }
                    emit([hashTag, doc.updated || doc.published]);
                  }
                });
            } else {
              const tag: any = doc.tag;
              if (tag.type === 'Hashtag') {
                let hashTag: string = tag.name.trim().toLowerCase();
                if (hashTag.startsWith('#')) {
                  hashTag = hashTag.slice(1);
                }
                emit([hashTag, doc.updated || doc.published]);
              }
            }
          }
        }
      }
    },
    language: 'javascript'
  };
}

async function updateDbSchema(user: User): Promise<void> {
  const actorId = getActorDocumentIdFromActorId(user.userId);
  await Couchdb.updateDbSchema(user, userDesignDocDef('actorId'),
    (source: string): string =>
      source.split('actorId').join(`'${actorId}'`),
    contentDatabaseUpdates
  );
}

const getUsersDatabase = async (user: User): Promise<Nano.DocumentScope<any>> => {
  await updateDbSchema(user);
  return Couchdb.nano.db.use(user.userDb);
};

async function collectionOf(user: User, id: string, collectionName: string): Promise<Collection<ASObject>> {
  const db = await getUsersDatabase(user);
  // assumes keys are in the format of [id, collectionName, publishDate]
  // publishDate can be null
  const collectionOfActors = ['followers', 'following', 'liked'];
  const replyMap = (row: any): ObjectRef<ASObject> => row.doc;
  const actorMap = (row: any): ActorRef => row.doc.actor;
  const startKey = [id, collectionName, '0000'];
  const endKey = [id, collectionName, '9999'];
  // eslint-disable-next-line @typescript-eslint/camelcase
  const result = await db.view(userDesignDoc, objectCollectionsView, {
    include_docs: true,
    startkey: startKey,
    endkey: endKey
  });
  const itemsAreActors = collectionOfActors.includes(collectionName);
  let items: ObjectRef<ASObject>[] = result.rows.map(itemsAreActors ? actorMap : replyMap);
  if (itemsAreActors) {
    items = Array.from(new Set(items));
  }
  return {
    id: `${id}/${collectionName}`,
    type: ObjectType.Collection,
    totalItems: result.rows.length,
    items: items as ObjectRefs<ASObject>
  };
}

async function activityOf(user: User, id: string): Promise<ASObject[]> {
  const db = await getUsersDatabase(user);
  const params: DocumentViewParams = {
    include_docs: true,
    key: id
  };
  const result = await db.view<ASObject>(userDesignDoc, 'activityOf', params);
  const objects = result.rows.map((row: any) => row.doc);
  return _.groupBy(objects, (object: ASObject) => object.type);
}

async function activityFrom(user: User, id: string): Promise<ASObject[]> {
  const db = await getUsersDatabase(user);
  const params: DocumentViewParams = {
    include_docs: true,
    key: id
  };
  const result = await db.view<ASObject>(userDesignDoc, 'activityFrom', params);
  const objects = result.rows.map((row: any) => row.doc);
  return _.groupBy(objects, (target: ASObject) => target.type);
}

async function relationship(user: User, actorId: string): Promise<Relationship> {
  const db = await getUsersDatabase(user);
  const params: DocumentViewParams = {
    include_docs: true,
    key: actorId
  };
  const result = await db.view<ASObject>(userDesignDoc, 'relationship', params);
  if (result.total_rows === 1 && result.rows[0]) {
    return result.rows[0].doc;
  }
  return null;
}

async function tags(user: User, tag: string, index: number, length: number): Promise<Nano.DocumentViewResponse<string, ASObject>> {
  const db = await getUsersDatabase(user);
  const params: DocumentViewParams = {
    include_docs: true,
    start_key: [tag, '9999'],
    end_key: [tag, '0000'],
    descending: true,
    skip: index,
    limit: length
  };
  return await db.view<string>(userDesignDoc, 'tagToObject', params);
}

function createTransformRequest(user: User, keyId: string, destination: string) {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return function (data: any, headers: any): any {
    // reverse-engineered from the http-headers source code
    const req = {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      setHeader: (header: any, value: any): void => headers[header] = value,
      getHeader: (header: string): void =>
        headers[_.find(Object.keys(headers), (t: string) => t.toLowerCase() === header.toLowerCase())],
      method: 'POST',
      path: new URL(destination).pathname,
      hasOwnProperty: () => false
    };
    // noinspection TypeScriptValidateJSTypes
    HttpSignature.sign(req, {
      authorizationHeaderName: 'Signature',
      key: user.privateKey,
      keyId,
      headers: ['date']
    });

    return data;
  };
}

function validateIdProtocol(id: string): boolean {
  const url = new URL(id);
  if (url.protocol !== 'https:') {
    console.warn(`not fetching object with ${url.protocol} protocol: ${id}`);
    return false;
  }
  return true;
}

async function fetchRemoteObject(id: string, user?: User): Promise<ASObject> {
  try {
    if (!validateIdProtocol(id)) {
      return null;
    }

    const params = {
      headers: {
        'Accept': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
      },
      timeout: 30000
    };
    if (user) {
      const keyId = getActorDocumentIdFromActorId(user.userId) + '#main-key';
      // @ts-ignore
      params.transformRequest = createTransformRequest(user, keyId, id);
    }
    const result: any = await Axios.get(id, params);

    return result.data;
  } catch (e) {
    if (e.response && e.response.status != 410) {
      console.info(`unable to fetch object ${id}: error ${e.message}`);
    }
    return null;
  }
}

async function fetchProperties(object: ASObject, fetchInstructions: FetchInstructions, user: User): Promise<ASObject> {
  const promises: Promise<any>[] = [];
  for (const property of Object.keys(fetchInstructions.fetch)) {
    const instruction = fetchInstructions.fetch[property];
    const propertyValue = object[property];
    switch (instruction) {
      case FetchInstruction.PopulateCollection:
        // noinspection JSUnusedLocalSymbols
        promises.push(collectionOf(user, object.id, property)
          .then((collection: Collection<ASObject>) => object[property] = collection)
          .catch((e: any) => {
          }));
        break;

      case FetchInstruction.Resolve:
        if (propertyValue) {
          promises.push(populateObjects(object, property, null, user));
        }
        break;

      default:
        let instructions: FetchInstructions;
        if ((instruction as FetchInstructions).recursive) {
          instructions = fetchInstructions;
        } else {
          instructions = instruction as FetchInstructions;
        }
        if ((instruction as FetchInstructions).type === 'Collection') {
          // noinspection JSUnusedLocalSymbols
          promises.push(collectionOf(user, object.id, property)
            .then(async (result: Collection<ASObject>) => {
              object[property] = result;
              const items: ASObject[] = result.items || result.orderedItems;
              await Promise.all(items.map(async (item: ASObject): Promise<ASObject> => {
                return fetchProperties(item, instructions, user);
              }));
            }).catch(e => {
            }));
        } else {
          if (object[property]) {
            promises.push(resolveObject(object[property], null, user).then(async (childObject: ASObject) => {
              object[property] = childObject;
              if (childObject) {
                await fetchProperties(childObject, instructions, user);
              }
            }));
          }
        }
    }
  }
  await Promise.all(promises);
  return object;
}

interface CacheRecord<T> {
  object: T,
  cached: number // time put into the cache
}

async function fetchFromCache<T>(id: string): Promise<CacheRecord<T>> {
  return JSON.parse(await ioredis.get(id));
}

async function cacheObject(id: string, object: any, ttl = 3600): Promise<void> {
  if (object && (!object.type || !CollectionSubTypes.includes(object.type))) {
    const cacheRecord: CacheRecord<any> = {
      object,
      cached: Date.now()
    };
    await ioredis.set(id, JSON.stringify(cacheRecord), 'EX', ttl);
  }
}

async function fetchObject(id: string, fetchInstructions?: FetchInstructions, user?: User): Promise<ASObject> {
  let object: ASObject = undefined;
  if (user) {
    object = await getById(user, id);
  }
  if (!object) {
    const cacheRecord = await fetchFromCache<ASObject>(id);
    if (!cacheRecord || !cacheRecord.object) {
      if (!cacheRecord || (cacheRecord && cacheRecord.cached + 6000 > Date.now())) {
        object = await fetchRemoteObject(id, user);
        await cacheObject(id, object);
      } else if (cacheRecord) {
        object = cacheRecord.object;
      }
    } else {
      object = cacheRecord.object;
    }

  }
  if (object && fetchInstructions) {
    await fetchProperties(object, fetchInstructions, user);
  }
  return object;
}

async function resolveLink(linkRef: LinkRef, user?: User): Promise<ASObject> {
  const key = resolveLinkRef(linkRef);
  return fetchObject(key, null, user);
}

async function resolveObject(objectRef: ObjectRef<ASObject>, fetchInstructions?: FetchInstructions, user?: User): Promise<ASObject> {
  if (objectRefType(objectRef) === 'ASObject') {
    return objectRef as ASObject;
  }
  return await fetchObject(resolveObjectRef(objectRef), fetchInstructions, user);
}

function resolveObjects(objectRefs: ObjectRefs<ASObject>, fetchInstructions?: FetchInstructions, user?: User): Promise<ObjectRefs<ASObject>> {
  if (objectRefsType(objectRefs) === 'array') {
    const promises: Promise<ASObject>[] = [];
    const refs = objectRefs as ObjectRef<ASObject>[];
    refs.forEach((ref: ObjectRef<ASObject>) => {
      promises.push(resolveObject(ref, fetchInstructions, user));
    });
    // @ts-ignore
    return Promise.all(promises);
  }
  return resolveObject(objectRefs as ObjectRef<ASObject>, fetchInstructions, user);
}

function resolveActor(actorRef: ActorRef, user?: User): Promise<Actor> {
  const actorUrl: string = resolveActorRef(actorRef);
  return fetchObject(actorUrl, null, user) as Promise<Actor>;
}

async function getById(user: User, key: string): Promise<ASObject> {
  try {
    const db = await getUsersDatabase(user);
    return await db.get(key);
  } catch (e) {
    if (e.statusCode === 404) {
      return null;
    }
    throw e;
  }
}

async function save(user: User, key: string, document: any, rev?: string): Promise<Nano.DocumentInsertResponse> {
  document._id = key;
  if (rev) {
    document._rev = rev;
  }
  const db = await getUsersDatabase(user);
  await cacheObject(key, document, 30);
  return db.insert(document);
}

function getActorDocumentIdFromActorId(userId: string): string {
  let baseUrl;
  if (Config.app) {
    baseUrl = Config.app.baseUrl;
  } else {
    baseUrl = 'testenv';
  }
  return `${baseUrl}/a/${userId}`;
}

async function populateObjects(object: ASObject, objectsName: string, fetchInstructions?: FetchInstructions, user?: User): Promise<void> {
  object[objectsName] = await
    resolveObjects(object[objectsName] as ObjectRefs<ASObject>, fetchInstructions, user);
}

  // retrieve the object from the remote system; store it in the user's database
exports.fetchRemoteObject = fetchRemoteObject;

  // retrieve the object from the user's database
exports.fetchObject = fetchObject

exports.fetchProperties = fetchProperties

exports.resolveLink = resolveLink

exports.resolveObject = resolveObject

exports.resolveObjects = resolveObjects

exports.resolveActor = resolveActor

exports.resolveActorRef = resolveActorRef

exports.activityOf = activityOf
exports.activityFrom = activityFrom
exports.relationship = relationship
exports.tags = tags
exports.populateCollectionsFor = async (user: User, object: APObject): Promise<void> => {
  [object.likes, object.dislikes, object.shares, object.replies] = await Promise.all([
    collectionOf(user, object.id, 'likes'),
    collectionOf(user, object.id, 'dislikes'),
    collectionOf(user, object.id, 'shares'),
    collectionOf(user, object.id, 'replies')
  ]);
  await Promise.all(
    ((object.replies as Collection<APObject>).items as APObject[]).map((item: APObject) =>
      Promise.all([module.exports.populateCollectionsFor(user, item),
        populateObjects(item, 'attributedTo', null, user)])));
};

exports.populateObjects = populateObjects;

// given a user and one or more collection names (e.g., 'likes', 'followers'), return an array of promises for
// each.
exports.getCollectionsForActor = async (user: User, ...collectionNames: string[]): Promise<[Collection<ASObject>]> => {
  // @ts-ignore
  return Promise.all(collectionNames.map((collectionName: string) => module.exports.getCollectionForActor(user, collectionName)));
};

exports.getCollectionForActor = async (user: User, collectionName: string): Promise<Collection<ASObject>> => {
  const key = getActorDocumentIdFromActorId(user.userId);
  return collectionOf(user, key, collectionName);
};

exports.getCollectionForObject = (user: User, objectId: string, collectionName: string): Promise<Collection<ASObject>> => {
  return collectionOf(user, objectId, collectionName);
};

exports.getObjectFromActivity = async (user: User, key: string): Promise<ASObject> => {
  const db = await getUsersDatabase(user);
  // eslint-disable-next-line @typescript-eslint/camelcase
  const result = await db.view(userDesignDoc, activityIdToObjectIdView, {include_docs: true, key});
  if (result && result.rows && result.rows[0]) {
    const activity: Activity = result.rows[0].doc;
    const refType = objectRefType(activity.object);
    if (refType == 'ASObject') {
      return activity.object as ASObject;
    }
  }
  return null;
};

exports.createContentDatabases = async (user: User): Promise<User> => {
  const userDbName = user.userId;
  await Couchdb.nano.db.create(userDbName);
  user.userDb = userDbName;
  await updateDbSchema(user);
  return user;
};

exports.save = save;

exports.docHeaders = async (user: User, key: string): Promise<{ [index: string]: string }> => {
  const db = await getUsersDatabase(user);
  return db.head(key);
};

exports.rev = async (user: User, key: string): Promise<string> => {
  try {
    return (await module.exports.docHeaders(user, key))['ETag'];
  } catch (e) {
    return null;
  }
};

exports.timeline = async (user: User, limit: number, offset?: number): Promise<Nano.DocumentViewResponse<string, ASObject>> => {
  const db = await getUsersDatabase(user);
  const params: DocumentViewParams = {
    descending: true,
    include_docs: true,
    limit
  };
  if (offset) {
    params.skip = offset;
  }
  return db.view(userDesignDoc, timelineView, params);
};

exports.publicTimeline = async (user: User, index: number = 0, length: number = 10): Promise<Nano.DocumentViewResponse<string, ASObject>> => {
  const db = await getUsersDatabase(user);
  const params: DocumentViewParams = {
    // eslint-disable-next-line @typescript-eslint/camelcase
    include_docs: true,
    descending: true,
    limit: length,
    skip: index
  };
  return db.view(userDesignDoc, publicTimelineView, params);
};

exports.postIdForObjectId = async (user: User, key: string): Promise<Nano.DocumentViewResponse<string, ASObject>> => {
  const db = await getUsersDatabase(user);
  // eslint-disable-next-line @typescript-eslint/camelcase
  return db.view(userDesignDoc, contentToPostIdView, {include_docs: true, key});
};

exports.getById = getById;

exports.deleteDocument = async (user: User, key: string, rev?: string): Promise<Nano.DocumentDestroyResponse> => {
  const db = await getUsersDatabase(user);
  if (!rev) {
    const doc = await module.exports.rev(user, key);
    rev = doc._rev;
  }
  return db.destroy(key, rev);
};

  // moved here to break circular dependency with Users.
exports.getActorDocumentIdFromActorId = getActorDocumentIdFromActorId;

const contentDatabaseUpdates: DatabaseUpdate[] = [
  // {
  //   id: '2020-05-06-01',
  //   name: 'embedAcceptObjects',
  //   code: async (user: User, db: nano.DocumentScope<any>): Promise<void> => {
  //     const results = (await db.list({ include_docs: true })).rows.map((doc: any) => doc.doc);
  //     const accepts = results.filter((doc: any): boolean => doc.type === ObjectType.Accept);
  //     const promises = accepts.map(async (accept: any): Promise<void> => {
  //       try {
  //         if (accept.object) {
  //           accept.object = await resolveObject(accept.object, null, user)
  //           // await db.insert(accept);
  //         }
  //       } catch (e) {
  //         console.error(e);
  //       }
  //     });
  //     await Promise.all(promises);
  //   }
  // }
];
