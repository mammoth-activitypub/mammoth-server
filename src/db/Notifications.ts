'use strict';

import {User} from '../struct/User';
import {Notification} from 'mammoth-api';
const {NotificationType} = require('mammoth-api');
import * as nano from 'nano';
import {DocumentViewParams, DocumentViewResponse} from 'nano';
import {Activity, ASObject} from 'mammoth-activitystreams';
const {ObjectType, resolveActorRefs, resolveObjectRef} = require('mammoth-activitystreams');

const Couchdb = require('./couchdb');
let Content = require('./content');
const v4 = require('uuid').v4;
const Boom = require('@hapi/boom');


const userDesignDoc = 'notifications_doc';

function emit(...vars: any[]): any {
  return vars;
}

function sum(_foo: any): number { return _foo; }

function userDesignDocDef(): any {
  return {
    _id: '_design/' + userDesignDoc,
    views: {
      // notificationAggregations: {
      //   map: function (doc: Notification): void {
      //     if (doc.type === 'Notification') {
      //       emit([doc.aggregation, doc.published]);
      //     }
      //   },
      //   reduce: function(keys: string[], values: string[], rereduce: boolean): string {
      //     let max = '0000';
      //     keys.forEach((key: string) => {
      //       if (key.localeCompare(max) > 0) {
      //         max = key;
      //       }
      //     })
      //     return max;
      //   }
      // },
      notifications: {
        map: function (doc: Notification): void {
          if (doc.type === 'Notification') {
            emit(doc.published);
          }
        }
      },
      notificationsAlready: {
        map: function (doc: Notification): void {
          if (doc.type === 'Notification') {
            emit([doc.activity, doc.actor, doc.target]);
          }
        }
      },
      unreadNotifications: {
        map: function (doc: Notification): void {
          if (doc.type === 'Notification' && !doc.read) {
            emit(doc.id);
          }
        },
        reduce: function (keys: any, values: any, rereduce: boolean): number {
          if (rereduce) {
            return sum(values);
          } else {
            return values.length;
          }
        }
      }
    },
    language: 'javascript'
  };
}

async function updateDbSchema(user: User): Promise<void> {
  await Couchdb.updateDbSchema(user, userDesignDocDef());
}

async function getUsersDatabase(user: User): Promise<nano.DocumentScope<any>> {
  await updateDbSchema(user);
  return Couchdb.nano.db.use(user.userDb);
}

exports.getNotifications = async function(user: User, offset = 0, length = 5): Promise<DocumentViewResponse<Notification, any>> {
  const db = await getUsersDatabase(user);
  const params: DocumentViewParams = {
    // eslint-disable-next-line @typescript-eslint/camelcase
    include_docs: true,
    skip: offset,
    limit: length,
    descending: true
  };
  return db.view<Notification>(userDesignDoc, 'notifications', params);
}

exports.getUnreadNotificationCount = async function(user: User): Promise<DocumentViewResponse<number, any>> {
  const db = await getUsersDatabase(user);
  const params: DocumentViewParams = {
    // eslint-disable-next-line @typescript-eslint/camelcase
    include_docs: false,
    reduce: true
  };
  return db.view(userDesignDoc, 'unreadNotifications', params);
}

async function getById(user: User, key: string): Promise<Notification> {
  try {
    const db = await getUsersDatabase(user);
    return await db.get(key);
  } catch (e) {
    if (e.statusCode === 404) {
      return null;
    }
    throw e;
  }
}

async function save(user: User, key: string, document: any, rev?: string): Promise<nano.DocumentInsertResponse> {
  document._id = key;
  if (rev) {
    document._rev = rev;
  }
  const db = await getUsersDatabase(user);
  return db.insert(document);
}

exports.update = async function (user: User, notification: Notification): Promise<Notification> {
  const notificationToUpdate: Notification = await getById(user, notification.id);
  if (notificationToUpdate.type !== 'Notification') {
    throw Boom.badRequest('Object id ' + notification.id + ' is not a notification');
  }
  notificationToUpdate.read = notification.read;
  await save(user, notificationToUpdate.id, notificationToUpdate);
  return notificationToUpdate;
}

exports.markAllAsRead = async function(user: User): Promise<void> {
  const db = await getUsersDatabase(user);
  const params: DocumentViewParams = {
    // eslint-disable-next-line @typescript-eslint/camelcase
    include_docs: true,
    reduce: false
  };
  const unreadNotifications = await db.view(userDesignDoc, 'unreadNotifications', params);
  await Promise.all(unreadNotifications.rows.map(async (row) => {
    const notification = row.doc;
    notification.read = true;
    return save(user, notification.id, notification);
  }));
}

async function userNotifiedAlready(user: User, activity: string, actor: string | string[], target: string): Promise<boolean> {
  const db = await getUsersDatabase(user);
  const key = [activity, actor, target];
  const result = await db.view<Notification>(userDesignDoc, 'notificationsAlready', { key });
  return result.total_rows > 0;
}

function handleLikeOfSomethingUserDid(object: ASObject): string {
  if (object.inReplyTo) {
    return NotificationType.LikedUsersReply;
  }
  return NotificationType.LikedUsersPost;
}

async function handleAcceptOfSomethingUserDid(object: ASObject): Promise<string> {
  if (object.type === ObjectType.Follow) {
    return NotificationType.AcceptedUsersFollowRequest;
  }
  const objectTarget: ASObject = await Content.resolveObject(object.target);
  if (object.type === ObjectType.Invite && objectTarget && objectTarget.type === ObjectType.Relationship) {
    return NotificationType.AcceptedUsersFriendRequest;
  }
  return null;
}

function handleCreateInReplyToSomethingTheUserDid(object: ASObject): string {
  if (object.inReplyTo) {
    return NotificationType.RepliedToUsersReply;
  }
  return NotificationType.RepliedToUsersPost;
}

async function actedOnAnObjectUserCreated(user: User, activity: Activity): Promise<any> {
  const object = await Content.resolveObject(activity.object, null, user);
  if (object) {
    const objectActor = resolveActorRefs(object.actor || object.attributedTo);
    if (objectActor === Content.getActorDocumentIdFromActorId(user.userId)) {
      switch (activity.type) {
        case ObjectType.Like:
          return handleLikeOfSomethingUserDid(object);

        case ObjectType.Announce:
          return NotificationType.SharedUsersPost;

        case ObjectType.Accept:
          return handleAcceptOfSomethingUserDid(object);

        case ObjectType.Create:
          return handleCreateInReplyToSomethingTheUserDid(object);
      }
    }
  }
  return null;
}

async function actedOnTheUserActor(user: User, activity: Activity): Promise<string> {
  const objectRef = activity.object;
  const actorId = Content.getActorDocumentIdFromActorId(user.userId);
  if (resolveObjectRef(objectRef) !== actorId) {
    return null;
  }
  if (activity.type === ObjectType.Follow) {
    return NotificationType.SentUserAFollowRequest;
  }
  if (activity.type === ObjectType.Invite) {
    const target: ASObject = await Content.resolveObject(activity.target, null, user);
    if (target.type === ObjectType.Relationship) {
      return NotificationType.SentUserAFriendRequest;
    }
  }
  return null;
}

async function repliedToAnObjectUserCreated(user: User, activity: Activity): Promise<any> {
  if (!activity.inReplyTo) {
    return null;
  }
  const repliedObject = await Content.resolveObject(activity.inReplyTo);
  const repliedActor = resolveActorRefs(repliedObject.attributedTo);
  if (repliedActor === Content.getActorDocumentIdFromActorId(user.userId)) {
    return repliedObject.inReplyTo ? NotificationType.RepliedToUsersReply : NotificationType.RepliedToUsersPost;
  }
}

exports.shouldCreateNotification = async function(user: User, activity: Activity): Promise<any> {
  // if (await userNotifiedAlready(user, activity.type, resolveActorRefs(activity.actor), resolveObjectRef(activity.object))) {
  //   return null;
  // }

  switch (activity.type) {
    case ObjectType.Like:
    case ObjectType.Announce:
    case ObjectType.Accept:
    case ObjectType.TentativeAccept:
    case ObjectType.Reject:
    case ObjectType.TentativeReject:
      return actedOnAnObjectUserCreated(user, activity);

    case ObjectType.Create:
      return repliedToAnObjectUserCreated(user, activity);

    case ObjectType.Follow:
    case ObjectType.Invite:
      return await actedOnTheUserActor(user, activity);
  }
}

exports.createNotification = async function(user: User, activity: Activity, notificationType: any): Promise<void> {
  const actor = Content.getActorDocumentIdFromActorId(user.userId);
  const id = actor + '/' + v4();
  const notification: Notification = {
    id,
    type: 'Notification',
    published: activity.published || new Date().toISOString(),
    actor,
    activity: notificationType,
    object: resolveObjectRef(activity),
    target: resolveObjectRef(activity.object),
    aggregation: null,
    read: false
  }
  await Couchdb.save(user, id, notification);
}
