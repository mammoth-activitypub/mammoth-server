'use strict';
/// <reference types="jest" />
/// <reference types="rewire" />

import {ASObject, Follow, Like, Note, ObjectType} from 'mammoth-activitystreams';
import {User} from '../struct/User';

const rewire = require('rewire');

let Content = rewire('../../dist/db/content');
let userDesignDocDef = Content.__get__('userDesignDocDef');
let ioredis = {
  get: jest.fn(),
  set: jest.fn()
}

beforeEach(() => {
  Content = rewire('../../dist/db/content');
  Content.__set__('ioredis', ioredis);
  userDesignDocDef = Content.__get__('userDesignDocDef');
});

describe('couchdb views', () => {
  const emitMock = jest.fn();

  beforeEach(() => {
    Content.__set__('emit', emitMock);
  });

  test('timeline view map function emits published date on posts only', () => {

    const timelineMapFunction = userDesignDocDef('someUrl').views.timeline.map;

    const simplePost: Partial<Note> = {
      type: ObjectType.Note,
      published: "now"
    };

    const notAPost: Partial<Like> = {
      type: ObjectType.Like,
      published: "whenever"
    };

    timelineMapFunction(simplePost);
    timelineMapFunction(notAPost);

    expect(emitMock).toHaveBeenCalledWith('now');
  });

  test.skip('followers view emits both directions', () => {
    const follow: Follow = {
      id: "123",
      type: ObjectType.Follow,
      actor: "bob",
      object: "fred"
    };

    const objectCollectionsMapFunction = userDesignDocDef('someActor').views.objectCollections.map;

    objectCollectionsMapFunction(follow);
    expect(emitMock).toHaveBeenCalledWith(['bob', 'following', '9999']);
  });

});

describe('fetchRemoteObject tests', () => {
  let fetchRemoteObject: (id: string, user?: Partial<User>) => Promise<any>;
  const axiosGetMock = jest.fn();
  const contentSaveMock = jest.fn();
  const user: Partial<User> = {
    userId: 'bob'
  };
  const goodResult = { data: "ok" };

  beforeEach(() => {
    fetchRemoteObject = Content.__get__('fetchRemoteObject');
    Content.__set__('Axios', {get : axiosGetMock});
    Content.__set__('save', contentSaveMock);
    axiosGetMock.mockReset();
    contentSaveMock.mockReset();
  });

  test('fails gracefully', async () => {
    axiosGetMock.mockRejectedValue("that didn't go as planned");
    try {
      await fetchRemoteObject("somebadId");
      expect(0).toBe(1);
    } catch {
      expect(1).toBe(1);
    }
  });

  test('fails gracefully when notfound', async () => {
    axiosGetMock.mockRejectedValue({response: {status: 404}});
    expect(await fetchRemoteObject("https://someId")).toBeNull();
  });

  test('sets correct Accept header', async () => {
    axiosGetMock.mockResolvedValue(goodResult);
    await fetchRemoteObject('https://someUrl');

    const call = axiosGetMock.mock.calls[0][1];
    expect(call.headers['Accept']).toBe('application/ld+json; profile="https://www.w3.org/ns/activitystreams"');
  });

  test('sets reasonable timeout', async () => {
    axiosGetMock.mockResolvedValue(goodResult);
    await fetchRemoteObject('https://someUrl');

    const call = axiosGetMock.mock.calls[0][1];
    expect(call.timeout).toBeGreaterThan(2000);
    expect(call.timeout).toBeLessThan(30001);
  });

  test('does not save data when retrieved', async () => {
    axiosGetMock.mockResolvedValue(goodResult);
    const url = 'https://someurl';
    await fetchRemoteObject(url, user);

    expect(contentSaveMock).not.toHaveBeenCalledWith(user, url, goodResult.data);
  });

  test('doesn\'t save data when not needed', async () => {
    axiosGetMock.mockResolvedValue(goodResult);
    const url = 'https://someurl';
    await fetchRemoteObject(url);

    expect(contentSaveMock).not.toHaveBeenCalled();
  });

});

describe('fetchObject tests', () => {
  let fetchObject: (id: string, user?: Partial<User>) => Promise<any>;
  const user: Partial<User> = {
    userId: 'bob'
  };

  const goodResult = { id: "ok" };

  const fetchRemoteObjectMock = jest.fn();

  const contentGetMock = jest.fn();

  beforeEach(() => {
    Content.__set__('fetchRemoteObject', fetchRemoteObjectMock);
    Content.__set__('getById', contentGetMock);
    fetchObject = Content.__get__('fetchObject');
    contentGetMock.mockReset();
    fetchRemoteObjectMock.mockReset();
  });

  test('it tries to get object from the cache, and return it if found', async () => {
    contentGetMock.mockResolvedValue(goodResult);

    const result = await Content.fetchObject("someId", null, user);
    expect(result).toBe(goodResult);
    expect(contentGetMock).toHaveBeenCalledWith(user, "someId");
    expect(fetchRemoteObjectMock).not.toHaveBeenCalled();
  });

  test('it doesn\'t try to fetch from the cache if user not supplied', async () => {
    fetchRemoteObjectMock.mockResolvedValue(goodResult);
    ioredis.get.mockResolvedValue(null);

    const result = await Content.fetchObject("someId");

    expect(result).toBe(goodResult);
    expect(contentGetMock).not.toHaveBeenCalled();
    expect(fetchRemoteObjectMock).toHaveBeenCalledWith("someId", undefined);
  });
});

describe('resolveLink tests', () => {
  const fetchObjectMock = jest.fn();

  beforeEach(() => {
    Content.__set__('fetchObject', fetchObjectMock);
    fetchObjectMock.mockReset();
  });

  test('it fetches links from db when user is supplied', async () => {
    const expectedResult = { id: 'bob' };
    fetchObjectMock.mockResolvedValue(expectedResult);
    const id = "http://foo";

    const result = await Content.resolveLink(id);

    expect(result).toBe(expectedResult);
    expect(fetchObjectMock).toHaveBeenCalledWith(id, null, undefined);
  });
});

describe('resolveObject', () => {
  test('pass object returns it back', () => {
    const object: Partial<ASObject> = {
      id: 'id',
      type: ObjectType.Reject
    };
    return expect(Content.resolveObject(object)).resolves.toBe(object);
  });

  test('pass references fetches it', async () => {
    const fetchObject = jest.fn();
    const expectedValue = {"foo":"bar"};
    fetchObject.mockResolvedValue(expectedValue);
    Content.__set__('fetchObject', fetchObject);

    expect(await Content.resolveObject("id", null, { })).toEqual(expectedValue);
    expect(fetchObject).toHaveBeenCalledWith("id", null, {});
  });
});

describe('resolveActor', () => {
  test('it returns the actor', async () => {
    const fetchObject = jest.fn();
    const actor = { id: "foo" };
    const user = { userId: "bob" };
    fetchObject.mockResolvedValue(actor);
    Content.__set__("fetchObject", fetchObject);
    expect(await Content.resolveActor("foo", user)).toEqual(actor);
    expect(fetchObject).toHaveBeenCalledWith("foo", null, user);
  });
});

