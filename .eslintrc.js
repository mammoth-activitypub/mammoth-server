module.exports = {
  root: true,
  env: {
    es6: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2018
  },
  extends: [
    "plugin:@typescript-eslint/recommended",
    'prettier'
  ],
  // add your custom rules here
  rules: {
    '@typescript-eslint/no-var-requires': 0,
    '@typescript-eslint/no-use-before-define': 0,
    '@typescript-eslint/ban-ts-ignore': 0,
    '@typescript-eslint/no-explicit-any': 0,
    'prefer-const': 0
  },
  parser: "@typescript-eslint/parser",
  plugins: [
    'prettier'
  ]
}
