# Mammoth Server

*An ActivityPub-based server for C2S and S2S social networking.*

Check the [wiki](https://gitlab.koehn.com/mammoth/mammoth-server/-/wikis/home) for some installation instructions (suggestions welcome).

If you're looking to get a development environment up and running, you can use the `docker-compose.yml` file to get started; just remove the server and ui services and run those locally. 
