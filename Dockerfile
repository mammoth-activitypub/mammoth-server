FROM node:latest

FROM node:latest as build

USER node

COPY --chown=node . /node/

WORKDIR /node

RUN yarn && \
    ./node_modules/typescript/bin/tsc && \
    npm run test && \
    npm run build && \
    yarn install --production --ignore-scripts --prefer-offline

CMD npm run start
